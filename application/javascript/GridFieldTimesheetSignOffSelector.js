(function($){
    $(document).ready(function() {
        $.entwine('ss', function($) {
            $('.ss-gridfield-item[data-class="Timesheet"]').entwine({
                onclick: function(e) {
                    return true;
                },
                onmouseover: function() {
                    return false;
                },
                onmouseout: function() {
                    return false;
                }
            });
        });
    });
}(jQuery));