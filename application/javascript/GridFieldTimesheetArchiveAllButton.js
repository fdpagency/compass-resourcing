(function($){
    $(document).ready(function() {
        $.entwine('ss', function($) {
            $('#action_DeleteTimesheets').entwine({
                onclick: function(e) {
                    if (!confirm('Are you sure you wish to delete all the selected timesheets?')) {
						e.preventDefault();
						return false;
					}
					else {
						this._super(e);
					}
                }
            });
        });
    });
}(jQuery));