(function($){
    $(document).ready(function() {
        $.entwine('ss', function($) {
            $('#action_ArchiveTimesheets').entwine({
                onclick: function(e) {
                    if (!confirm('Are you sure you wish to archive all the listed signed off timesheets?')) {
						e.preventDefault();
						return false;
					}
					else {
						this._super(e);
					}
                }
            });
        });
    });
}(jQuery));