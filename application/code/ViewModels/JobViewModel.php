<?php


class JobViewModel extends ViewableData {
	
	public static function create() {
        $args = func_get_args();
        return new JobViewModel($args[0]);
    }
    
    public function __construct($job) {
		$this->_job = $job;
    }
	
	private $_job;
	
	public function Job() {
		return $this->_job;
	}
	
	private $_link;
	
	public function Link($action = '') {
		if (is_null($this->_link)) {
			$this->_link = Controller::curr()->Link(sprintf('%s-%d%s', $this->_job->Slug, $this->_job->ID, $action));
		}
		return $this->_link;
	}
}
