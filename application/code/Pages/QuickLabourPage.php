<?php


class QuickLabourPage extends Page {
    
    private static $db = array(
        'Recipients' => 'Text',
    	'ThankYouMessage' => 'HTMLText'
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
    	$fields->addFieldToTab(
    	    'Root.Form', 
    	    TextField::create('Recipients', 'Recipients of Quick Labour Emails (comma separated list)')
        );
    	$fields->addFieldToTab(
            'Root.Form', 
            HtmlEditorField::create('ThankYouMessage', 'Thank You Message')
        );
        return $fields;
    }
}


class QuickLabourPage_Controller extends Page_Controller {
    
    private static $allowed_actions = array(
        'QuickLabourForm'
    );
    
	public function QuickLabourForm() {
        return QuickLabourForm::create($this);
    }
    
    public function SubmitQuickLabour($data, $form) {
		$email = QuickLabourEmail::create(
		    new ArrayData($data), 
		    $this->Recipients
        );
		$email->send();
        $this->redirect($this->Link('?sent=1'));
    }
    
    public function Sent() {
        return array_key_exists('sent', $_GET);
    }
}