<?php


class HomePage extends Page {

	private static $db = array(
		'JobsHeader' => 'Varchar(200)'
	);
    private static $has_many = array(
        'Jobs' => 'HomePageJob',
		'Blocks' => 'HomePageBlock'
    );
    
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->addFieldToTab(
			'Root.Jobs',
			TextField::create('JobsHeader', 'Header Text')
		);
        $fields->addFieldToTab(
    	    'Root.Jobs',
    	    GridField::create('Jobs', 'Jobs', $this->Jobs(), GridFieldConfig::create()->addComponents(
    			new GridFieldToolbarHeader(),
    			new GridFieldAddNewButton('toolbar-header-right'),
    			new GridFieldSortableHeader(),
    			new GridFieldDataColumns(),
    			new GridFieldPaginator(100),
    			new GridFieldEditButton(),
    			new GridFieldDeleteAction(),
    			new GridFieldDetailForm(),
    			GridFieldOrderableRows::create('Index')
            ))
        );
		$fields->addFieldToTab(
    	    'Root.Blocks',
    	    GridField::create('Blocks', 'Blocks', $this->Blocks(), GridFieldConfig::create()->addComponents(
    			new GridFieldToolbarHeader(),
    			new GridFieldAddNewButton('toolbar-header-right'),
    			new GridFieldSortableHeader(),
    			new GridFieldDataColumns(),
    			new GridFieldPaginator(100),
    			new GridFieldEditButton(),
    			new GridFieldDeleteAction(),
    			new GridFieldDetailForm(),
    			GridFieldOrderableRows::create('Index')
            ))
        );
		return $fields;
	}
}

class HomePage_Controller extends Page_Controller {

}