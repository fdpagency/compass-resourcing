<?php


class ClientDashboardPage extends Page {
	
	private static $db = array(
		'LoginContent' => 'HTMLText'
	);
	
	public function canView($member = null) {
        if (is_subclass_of(Director::get_current_page(), 'CMSMain')) {
            return parent::canView($member);
        }
        if (!$member || !(is_a($member, 'Member')) || is_numeric($member)) {
            $member = Member::currentUser();
        }
        if (!is_null($member)) {
            $client = $member->Client();
            if (!is_null($client) && $client->exists()) {
                return true;
            }
        }
		$controller = Controller::curr();
        if (!is_a($controller, 'ClientDashboardPage_Controller')) {
			$controller->redirect($this->Link());
		}
		return true;
    }
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->addFieldToTab(
			'Root.Main',
			HTMLEditorField::create('LoginContent', 'Login Content'),
			'Metadata'
		);
		return $fields;
	}
}


class ClientDashboardPage_Controller extends Page_Controller {
	
	private static $allowed_actions = array(
		'LoginForm',
		'Logout'
	);
	
	private $_child_rows;
	
	public function ChildRows() {
		if (is_null($this->_child_rows)) {
			$this->_child_rows = ArrayList::create();
			$children = $this->Children();
			for ($i = 0; $i < round($children->count() / 3 + 0.5, 0); $i++) {
				$this->_child_rows->push($children->limit(3, $i * 3));
			}
		}
		return $this->_child_rows;
	}
	
	private $_authenticated;
	
	public function Authenticated() {
		if (is_null($this->_authenticated)) {
			$member = Member::currentUser();
			if (!is_null($member)) {
				$client = $member->Client();
				$this->_authenticated = (!is_null($client) && $client->exists());
			}
			else {
				$this->_authenticated = false;
			}
		}
		return $this->_authenticated;
	}
	
	public function LoginForm() {
		return singleton(Authenticator::get_default_authenticator())->get_login_form($this);
	}
	
	public function Logout(SS_HTTPRequest $request) {
		$member = Member::currentUser();
        if (!is_null($member)) {
            $member->logout();
        }
        $this->redirect($this->Link());
	}
}