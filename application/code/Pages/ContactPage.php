<?php

class ContactPage extends Page {

    private static $db = array(
        'ContentBottom' => 'HTMLText'
    );

    public static $has_one = array(
    );

    public static $has_many = array(
        'Offices'=>'Office'
    );

    function getCMSFields() {

        $fields = parent::getCMSFields();

        $gridConfigFeatured = GridFieldConfig::create()->addComponents(
            new GridFieldToolbarHeader(),
            new GridFieldAddNewButton('toolbar-header-right'),
            new GridFieldSortableHeader(),
            new GridFieldDataColumns(),
            new GridFieldPaginator(20),
            new GridFieldEditButton(),
            new GridFieldDeleteAction(),
            new GridFieldDetailForm(),
            new GridFieldSortableRows('SortOrder')
        );

        $gridFieldFeatured = new GridField("Offices", "Offices", $this->Offices(), $gridConfigFeatured);
        $fields->addFieldToTab("Root.Offices Address", $gridFieldFeatured);

        $fields->addFieldToTab('Root.Main', new HTMLEditorField('ContentBottom', 'Content Bottom'), 'Metadata');

        return $fields;
    }

}

class ContactPage_Controller extends Page_Controller {

    public static $allowed_actions = array (
    );

    public function init() {
        parent::init();
    }

}