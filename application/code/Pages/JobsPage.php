<?php


class JobsPage extends Page {
	
	private static $db = array(
		'ThankYouMessage' => 'HTMLText'
	);
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->addFieldToTab(
			'Root.ApplicationForm',
			HTMLEditorField::create('ThankYouMessage', 'Thank You Message')
		);
		return $fields;
	}
}


class JobsPage_Controller extends Page_Controller {
	
	private static $allowed_actions = array(
		'DisplayJob',
		'JobApplicationForm'
	);
	private static $url_handlers = array(
		'JobApplicationForm' => 'JobApplicationForm',
		'$Slug!' => 'DisplayJob'
	);
	
	private $_jobs;
	
	public function Jobs() {
		if (is_null($this->_jobs)) {
			$this->_jobs = Job::get()->filter(array(
				'Published' => true,
				'Expires:GreaterThanOrEqual' => date('Y-m-d')
			));
			$data = $this->getSearchData();
			if (array_key_exists('Keywords', $data) && !empty($data['Keywords'])) {
				$this->_jobs = $this->_jobs->filterAny(array(
					'Title:PartialMatch' => $data['Keywords'],
					'Keywords:PartialMatch' => $data['Keywords'],
					'Description:PartialMatch' => $data['Keywords']
				));
			}
			if (array_key_exists('Location', $data) && !empty($data['Location'])) {
				$location = Geocoder::geocode($data['Location']);
				if (!is_null($location)) {
					$this->_jobs = $this->_jobs->innerJoin(sprintf("
						(
							SELECT 
								`Job`.`ID`,
								(ACOS(SIN(RADIANS(`Job`.`Latitude`)) * SIN(RADIANS(%f)) + COS(RADIANS(`Job`.`Latitude`)) * COS(RADIANS(%f)) * COS(RADIANS(`Job`.`Longitude`) - RADIANS(%f))) * 6378.1) * 0.62137119 AS `Distance`
							FROM `Job`
						)", $location->lat, $location->lat, $location->lng), 
						'JobDistance.ID = Job.ID', 
						'JobDistance'
					)->where('Distance < 20');
				}
			}
			if (array_key_exists('MinPay', $data) && !empty($data['MinPay'])) {
				$this->_jobs = $this->_jobs->filter('MinPay:GreaterThanOrEqual', $data['MinPay']);
			}
			if (array_key_exists('MaxPay', $data) && !empty($data['MaxPay'])) {
				$this->_jobs = $this->_jobs->filter('MaxPay:LessThanOrEqual', $data['MaxPay']);
			}
			if (array_key_exists('PaySchedule', $data) && !empty($data['PaySchedule']) && (array_key_exists('MinPay', $data) && !empty($data['MinPay'])) || (array_key_exists('MaxPay', $data) && !empty($data['MaxPay']))) {
				$this->_jobs = $this->_jobs->filter('PaySchedule', $data['PaySchedule']);
			}
			if (array_key_exists('JobType', $data) && !empty($data['JobType']) && count($data['JobType']) > 0) {
				$this->_jobs = $this->_jobs->filter('JobType', $data['JobType']);
			}
			if (array_key_exists('EmploymentType', $data) && !empty($data['EmploymentType']) && count($data['EmploymentType']) > 0) {
				$this->_jobs = $this->_jobs->filter('EmploymentType', $data['EmploymentType']);
			}
		}
		return $this->_jobs;
	}
	
	private $_job;
	
	public function Job()  {
		return $this->_job;
	}
	
	public function Sent() {
		return array_key_exists('sent', $this->request->getVars());
	}
	
	public function init() {
		parent::init();
		if ($this->request->isPOST() && $id = $this->request->postVar('JobID')) {
			$this->_job = $this->Jobs()->filter('ID', $id)->first();
			if (is_null($this->_job)) {
				throw new SS_HTTPResponse_Exception(ErrorPage::response_for(404), 404);
			}
		}
	}
	
	public function DisplayJob(SS_HTTPRequest $request) {
		$job = null;
		if ($slug = $request->param('Slug')) {
			if (preg_match('/^([a-z0-9\-]*)\-([0-9]*)$/', $slug, $match)) {
				$job = $this->Jobs()->filter('ID', $match[2])->first();
				if (!is_null($job) && $job->Slug != $match[1]) {
					$this->redirect(JobViewModel::create($job)->Link());
				}
				$this->_job = $job;
			}
		}
		if (is_null($job)) {
			throw new SS_HTTPResponse_Exception(ErrorPage::response_for(404), 404);
		}
		return array();
	}
	
	public function JobApplicationForm() {
		return JobApplicationForm::create($this, $this->_job);
	}
	
	public function SubmitApplication($data, $form) {
		$email = JobApplicationEmail::create(
		    new ArrayData(array_merge($data, array(
				'Job' => $this->_job
			)))
        );
		$email->send();
        $this->redirect(JobViewModel::create($this->_job)->Link('?sent=1'));
    }
}