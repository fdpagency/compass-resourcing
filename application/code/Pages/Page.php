<?php


class Page extends SiteTree {

	private static $db = array(
    	'HeaderNav' => 'Boolean',
    	'MainNav' => 'Boolean'
	);
	
	public function getSettingsFields() {
    	$fields = parent::getSettingsFields();
    	$fields->addFieldToTab('Root.Settings', CheckBoxField::create('HeaderNav', 'Show in header navigation?'), 'ShowInSearch');
    	$fields->addFieldToTab('Root.Settings', CheckBoxField::create('MainNav', 'Show in main navigation?'), 'ShowInSearch');
    	return $fields;
	}
	
	private static $_tree;
	
	protected static function _get_tree() {
    	if (is_null(self::$_tree)) {
        	$controller = Controller::curr();
        	self::$_tree = is_a($controller, 'Page_Controller') ? $controller->Crumbs() : false;
    	}
    	return self::$_tree;
	}
	
	private $_crumbs;
	
	public function Crumbs($maxDepth = 20, $unlinked = false, $stopAtPageType = false, $showHidden = true) {
        if (is_null($this->_crumbs)) {
            $page = $this;
            $pages = array();
            while ($page && (!$maxDepth || count($pages) < $maxDepth) && (!$stopAtPageType || $page->ClassName != $stopAtPageType)) {
                if ($showHidden || $page->ShowInMenus || ($page->ID == $this->ID)) { 
                    $pages[] = $page;
                }
                $page = $page->Parent;
            }    
            $this->_crumbs = new ArrayList(array_reverse($pages));
        }
        return $this->_crumbs;
    }
    
    public function Current() {
        $tree = self::_get_tree();
        if ($tree) {
            foreach ($tree as $branch) {
                if ($branch->ID == $this->ID) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public function Link($action = null) {
        return is_null($this->_link) ? parent::Link($action) : $this->_link;
    }
    
    public function setLink($link) {
        $this->_link = $link;
    }
}

class Page_Controller extends ContentController {

	private static $allowed_actions = array(
        'JobSearchForm'
	);

	public function init() {
		parent::init();
		if ($this->currentUser() && $this->ClassName != 'TermsPage') {
			$page = $this->TermsPage();
			if (!is_null($page) && $this->currentUser()->ClientID > 0 && !$this->currentUser()->termsUpToDate()) {
				Controller::curr()->redirect($page->Link());
			}
		}
	}
	
	public function SiteBaseURL() {
		return Director::absoluteBaseURL();
	}
	
	public function getMenu($level = 1) {
        if($level == 1) {
			$result = Page::get()->filter(array(
				"ShowInMenus" => 1,
				"ParentID" => 0
			));

		} else {
			$parent = $this->data();
			$stack = array($parent);
			
			if($parent) {
				while($parent = $parent->Parent) {
					array_unshift($stack, $parent);
				}
			}
			
			if(isset($stack[$level-2])) $result = $stack[$level-2]->Children();
		}
        return $result;
    }
    
    private $_navigations = array();
    
    public function Navigation($type = 'Main', $level = 1) {
        $key = sprintf('%s_%d', $type, $level);
        if (!array_key_exists($key, $this->_navigations)) {
            $nav = $this->getMenu($level)->filter(array(
                sprintf('%sNav', $type) => true
            ));
            $this->_navigations[$key] = $nav;
        }
        return $this->_navigations[$key];
    }
    
    private $_query_string;
    
    public function QueryString() {
        if (is_null($this->_query_string)) {
            $vars = $this->request->getVars();
            if (array_key_exists('url', $vars)) {
                unset($vars['url']);
            }
            if (array_key_exists('start', $vars)) {
                unset($vars['start']);
            }
            if (count($vars) > 0) {
                $query = array();
                foreach ($vars as $key => $value) {
                    $query[] = sprintf('%s=%s', $key, urlencode($value));
                }
                $this->_query_string = sprintf('?%s', implode('&', $query));
            }
            else {
                $this->_query_string = '';
            }
        }
        return $this->_query_string;
    }
	
	private static $_search_session_key = 'JobSearch';
	
	private $_search_data;
	
	public function getSearchData() {
		if (is_null($this->_search_data)) {
			$vars = $this->request->getVars();
			if (array_key_exists('action_Reset', $vars)) {
				Session::clear(self::$_search_session_key);
				$this->redirect($this->request->getVar('url'));
			}
			else if (array_key_exists('action_Search', $vars)) {
				Session::set(self::$_search_session_key, $vars);
				$this->_search_data = $vars;
			}
			else {
				$this->_search_data = Session::get(self::$_search_session_key);
			}
			if (is_null($this->_search_data)) {
				$this->_search_data = array();
			}
		}
		return $this->_search_data;
	}

    public function JobSearchForm() {
		return JobSearchForm::create($this);
	}
	
	public function PartialJobSearchForm() {
		return JobSearchForm::create($this, false);
	}
    
    public function getPageByClass($class = 'Page') {
        return DataObject::get($class, '', '', '', '');
    }
	
	private $_client_dashboard_page;
	
	public function ClientDashboardPage() {
		if (is_null($this->_client_dashboard_page)) {
			$this->_client_dashboard_page = ClientDashboardPage::get()->first();
		}
		return $this->_client_dashboard_page;
	}
	
	private $_terms_page;
	
	public function TermsPage() {
		if (is_null($this->_terms_page)) {
			$this->_terms_page = TermsPage::get()->first();
		}
		return $this->_terms_page;
	}
	
	private $_jobs_page;
	
	public function JobsPage() {
		if (is_null($this->_jobs_page)) {
			$this->_jobs_page = JobsPage::get()->first();
		}
		return $this->_jobs_page;
    }
	
	private $_member;
	
	public function currentUser() {
		if (is_null($this->_member)) {
			if ($member = Member::currentUser()) {
				if (!is_a($member, 'Member')) {
					$this->_member = Member::get()->filter('ID', $member)->first();
				}
				else {
					$this->_member = $member;
				}
			}
			else {
				$this->_member = false;
			}
		}
		return $this->_member;
	}
}
