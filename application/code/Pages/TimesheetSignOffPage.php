<?php


class TimesheetSignOffPage extends Page {
    
    
}


class TimesheetSignOffPage_Controller extends Page_Controller {
    
    private static $allowed_actions = array(
        'Approved',
        'ViewTimesheet',
        'TimesheetApprovalForm',
        'TimesheetForm',
        'Approve'
    );
    private static $url_handlers = array(
        'TimesheetApprovalForm' => 'TimesheetApprovalForm',
        'TimesheetForm' => 'TimesheetForm',
        'approved/timesheet/$ID!' => 'ViewTimesheet',
		'approved' => 'Approved',
        'timesheet/$ID!' => 'ViewTimesheet'
    );
    
    private $_timesheets;
    
    public function Timesheets() {
        if (is_null($this->_timesheets)) {
            $member = Member::currentUser();
            $this->_timesheets = Timesheet::get()->filterAny(array(
                'SignOff1ID' => $member->ID,
				'SignOff2ID' => $member->ID
            ));
			if (!is_null($this->_show_approved)) {
				$this->_timesheets = $this->_timesheets->where(
					$this->_show_approved
					?
					sprintf(
						'SignOff1ID = %d AND SignedOff1 IS NOT NULL OR SignOff2ID = %d AND SignedOff2 IS NOT NULL',
						$member->ID,
						$member->ID
					)
					:
					sprintf(
						'SignOff1ID = %d AND SignedOff1 IS NULL OR SignOff2ID = %d AND SignedOff2 IS NULL',
						$member->ID,
						$member->ID
					)
				);
			}
        }
        return $this->_timesheets;
    }
    
    private $_timesheet;
    
    public function Timesheet() {
        return $this->_timesheet;
    }
    
    public function setTimesheet($data) {
        if (array_key_exists('ID', $data) && !empty($data['ID'])) {
            $this->_timesheet = $this->Timesheets()->filter('ID', $data['ID'])->first();
        }
        if (is_null($this->_timesheet)) {
            throw new SS_HTTPResponse_Exception(ErrorPage::response_for(404), 404);
        }
    }
    
    private $_show_approved = false;
    
    public function ShowApproved() {
        return $this->_show_approved;
    }
    
    public function Approved(SS_HTTPRequest $request) {
        $this->_show_approved = true;
        return array();
    }
    
    public function ViewTimesheet(SS_HTTPRequest $request) {
		$this->_show_approved = null;
        $this->setTimesheet($request->params());
        return array();
    }
    
    public function TimesheetApprovalForm() {
        return TimesheetApprovalForm::create($this, $this->Timesheets());
    }
    
    public function Approve($data, $form) {
        $this->setTimesheet($data);
        $timesheet = $this->Timesheet();
        $this->_approveTimesheet($timesheet);
        $this->redirect($this->Link());
    }
    
    public function TimesheetForm() {
        if ($this->request->isPOST()) {
            $this->setTimesheet($this->request->postVars());
        }
        return TimesheetForm::create($this, $this->_timesheet, array(
			'SaveTimesheet' => 'Save',
			'ApproveTimesheet' => 'Save & Approve'
		));
    }
    
	public function SaveTimesheet($data, $form) {
        $this->setTimesheet($data);
        $timesheet = $this->Timesheet();
        $form->saveInto($timesheet);
        $timesheet->write();
        $this->redirect($this->Link());
    }
	
    public function ApproveTimesheet($data, $form) {
        $this->setTimesheet($data);
        $timesheet = $this->Timesheet();
        $form->saveInto($timesheet);
        $this->_approveTimesheet($timesheet);
        $this->redirect($this->Link());
    }
    
    private function _approveTimesheet($timesheet) {
		$member = Member::currentUser();
		if ($timesheet->SignOff1ID == $member->ID) {
			$timesheet->SignedOff1 = date('Y-m-d H:i:s');
		}
        elseif ($timesheet->SignOff2ID == $member->ID) {
			$timesheet->SignedOff2 = date('Y-m-d H:i:s');
		}
        $timesheet->write();
        $page = TimesheetPage::get()->first();
        if (!is_null($page)) {
			$recipients = sprintf('%sRecipients', $timesheet->Type);
            $email = TimesheetApprovedEmail::create($timesheet, $page->$recipients);
            $email->send();
        }
    }
}