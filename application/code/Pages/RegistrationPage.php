<?php

class RegistrationPage extends Page {
    
    private static $db = array(
        'FooterNotice' => 'Text',
        'Recipients' => 'Text',
        'ThankYouMessage' => 'HTMLText',
        'Eligibility' => 'HTMLText',
        'PensionScheme' => 'HTMLText',
        'PAYEAgreement' => 'HTMLText',
        'FileUpload' => 'HTMLText',
        'LimitedCompanyProcedure' => 'HTMLText'
    );
    
    public function getCMSFields() {
    	$fields = parent::getCMSFields();
    	$fields->addFieldToTab(
    	    'Root.Main', 
    	    TextareaField::create('FooterNotice', 'Footer Notice'),
    	    'Metadata'
        );
    	$fields->addFieldToTab(
    	    'Root.Form', 
    	    TextField::create('Recipients', 'Recipients of registrations (comma separated list)')
        );
    	$fields->addFieldToTab(
    	    'Root.Form', 
    	    HtmlEditorField::create('ThankYouMessage', 'Thank You Message')
        );
        $fields->addFieldToTab(
            'Root.Eligibility',
            HtmlEditorField::create('Eligibility', 'Proof of Eligibility')
        );
        $fields->addFieldToTab(
            'Root.PensionScheme',
            HtmlEditorField::create('PensionScheme', 'Pension Scheme')
        );
        $fields->addFieldToTab(
            'Root.PAYE',
            HtmlEditorField::create('PAYEAgreement', 'Agreement')
        );
        $fields->addFieldToTab(
            'Root.Uploads',
            HtmlEditorField::create('FileUpload', 'Instructions')
        );
        $fields->addFieldToTab(
            'Root.Uploads',
            HtmlEditorField::create('LimitedCompanyProcedure', 'Limited Company Instructions')
        );
    	return $fields;
	}
}


class RegistrationPage_Controller extends Page_Controller {
    
    private static $allowed_actions = array(
        'RegistrationForm',
        'RegistrationType'
    );
    private static $url_handlers = array(
        'registrationform' => 'RegistrationForm',
        '$RegistrationType!' => 'RegistrationType'
    );
    
    private static $types = array(
        array(
            'ClassName' => 'RegistrationPack',
            'Title' => 'Registration Pack',
            'Slug' => 'registration-pack',
            'AdminEmail' => 'RegistrationPackEmail',
            'ThankYouEmail' => 'RegistrationPackThankYouEmail'
        ),
        array(
            'ClassName' => 'MedicalQuestionnaire',
            'Title' => 'Medical Questionnaire',
            'Slug' => 'medical-questionnaire',
            'AdminEmail' => 'MedicalQuestionnaireEmail',
            'ThankYouEmail' => 'MedicalQuestionnaireThankYouEmail'
        ),
        array(
            'ClassName' => 'HolidayRequest',
            'Title' => 'Holiday Request',
            'Slug' => 'holiday-request',
            'AdminEmail' => 'HolidayRequestEmail',
            'ThankYouEmail' => 'HolidayRequestThankYouEmail'
        )
    );
    
    private $_type;
    
    public function Type() {
        if (is_string($this->_type)) {
            foreach ($this->Types() as $type) {
                if ($type->ClassName == $this->_type) {
                    $this->_type = $type;
                }
            }
        }
        return $this->_type;
    }
    
    private $_types;
    
    public function Types() {
        $this->_types = ArrayList::create();
        foreach ($this->stat('types') as $type => $attrs) {
            $this->_types->push(ArrayData::create(array_merge($attrs, array(
                'Link' => $this->Link($attrs['Slug'])
            ))));
        }
        return $this->_types;
    }
    
    public function init() {
        parent::init();
        if ($type = $this->request->postVar('RegistrationType')) {
            $this->_type = $type;
        }
    }
    
    public function RegistrationType(SS_HTTPRequest $request) {
        if ($type_slug = $request->param('RegistrationType')) {
            foreach ($this->Types() as $type) {
                if ($type->Slug == $type_slug) {
                    $this->_type = $type->ClassName;
                }
            }
        }
        $registration = $this->Registration();
        if ($registration) {
            if ($registration->Section == 'Complete') {
                $registration->complete();
                $type = $this->Type();
                $email = call_user_func_array(array($type->AdminEmail, 'create'), array($registration, $this->Recipients));
                $email->send();
                $email = call_user_func_array(array($type->ThankYouEmail, 'create'), array($registration));
                if (!is_null($email)) {
                    $email->send();
                }
                if ($registration->ClassName == 'RegistrationPack') {
                    foreach ($registration->Files() as $file) {
                        $file->delete();
                    }
                }
            }
        }
        return array();
    }
    
    private $_registration;
    
    public function Registration() {
        if (is_null($this->_registration)) {
            if ($type = $this->Type()) {
                $this->_registration = singleton($type->ClassName)->retrieve();
            }
        }
        return $this->_registration;
    }
    
	public function RegistrationForm() {
        return RegistrationForm::create($this, $this->Registration());
    }
    
    public function NextSection($data, $form) {
        $registration = $this->Registration();
        $form->saveInto($registration);
        $registration->nextSection();
        $this->redirect($this->Type()->Link);
    }
    
    public function PreviousSection($data, $form) {
        $registration = $this->Registration();
        $registration->previousSection(); 
        $this->redirect($this->Type()->Link);
    }
    
    public function UploadFile($data, $form) {
        for ($i = 1; $i <= 5; $i++) {
            $upload = sprintf('Upload%d', $i);
            $description = sprintf('UploadDescription%d', $i);
            if (array_key_exists($upload, $data) && array_key_exists($description, $data)) {
                if (array_key_exists('tmp_name', $data[$upload]) && !empty($data[$upload]['tmp_name'])) {
                    $file = RegistrationPackFile::create(array(
                        'PackID' => $this->Registration()->ID,
                        'Name' => $data[$upload]['name'],
                        'Description' => $data[$description]
                    ));
                    $file->write();
                    $file->save(file_get_contents($data[$upload]['tmp_name']));
                }
            }
        }
        $this->redirect($this->Type()->Link);
    }
    
    public function Sent() {
        return array_key_exists('sent', $this->request->getVars());
    }
}