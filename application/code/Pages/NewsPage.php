<?php


class NewsPage extends Page {
    
    private static $singular_name = 'News Page';
    private static $plural_name = 'News Pages';
    private static $description = 'Displays news articles which can be filtered by date';
    private static $icon = null;
    
    private static $db = array(
        'ArticlesPerPage' => 'Int'
    );
    private static $has_many = array(
        'Articles' => 'NewsArticle'
    );
    private static $defaults = array(
        'ArticlesPerPage' => 10
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
		$fields->addFieldToTab('Root.Articles', GridField::create('Articles', 'Articles', $this->Articles(), GridFieldConfig::create()->addComponents(
			new GridFieldToolbarHeader(),
			new GridFieldAddNewButton('toolbar-header-right'),
			new GridFieldSortableHeader(),
			new GridFieldDataColumns(),
			new GridFieldPaginator(100),
			new GridFieldEditButton(),
			new GridFieldDeleteAction(),
			new GridFieldDetailForm()
		)));
		$fields->addFieldToTab(
		    'Root.Listing', 
		    TextField::create('ArticlesPerPage', 'Articles Per Page')
        );
        return $fields; 
    }
    
    protected function _Articles() {
        return NewsArticle::get()->filter(array(
            'NewsArticle.PageID' => $this->ID,
            'Date:LessThanOrEqual' => date('Y-m-d')
        ));
    }
    
    private $_recent;
    
    public function RecentArticles() {
        if (is_null($this->_recent)) {
            $this->_recent = $this->_Articles()->limit(2);
        }
        return $this->_recent;
    }
}

class NewsPage_Controller extends Page_Controller {
    
    private static $allowed_actions = array(
        'FilterMonth',
        'DisplayArticle'
    );
    private static $url_handlers = array(
        '$Year!/$Month!/$Day!/$Slug!' => 'DisplayArticle',
        '$Year!/$Month!' => 'FilterMonth'
    );
    
    private $_archive;
    
    public function Archive() {
        if (is_null($this->_archive)) {
            $query = $this->_Articles()->dataQuery()->query();
            $query->setSelect(array(
                'Month' => 'MONTH(NewsArticle.Date)',
                'Year' => 'YEAR(NewsArticle.Date)'
            ))->setGroupBy(array('Year', 'Month'));
            $this->_archive = new ArrayList();
            foreach($query->execute() as $month) {
                $date = new SS_Datetime();
                $date->setValue(sprintf('%s-%s-01', $month['Year'], $month['Month']));
                $this->_archive->push(new ArrayData(array(
                    'Date' => $date
                )));
            }
        }
        return $this->_archive;
    }
    
    public function DisplayArticle(SS_HTTPRequest $request) {
        $year = $request->param('Year');
        $month = $request->param('Month');
        $day = $request->param('Day');
        $slug = $request->param('Slug');
        $date = new SS_Datetime();
        $date->setValue(sprintf('%s-%s-%s', $year, $month, $day));
        $this->_article = $this->_Articles()->filter(array(
            'Slug' => $slug,
            'Date:PartialMatch' => $date->Format('Y-m-d')
        ))->First();
        if (is_null($this->_article)) {
			throw new SS_HTTPResponse_Exception(ErrorPage::response_for(404), 404);
		}
        return array();
    }
    
    public function FilterMonth(SS_HTTPRequest $request) {
        $year = $request->param('Year');
        $month = $request->param('Month');
        $date = new SS_Datetime();
        $date->setValue(sprintf('%s-%s-01', $year, $month));
        $this->_month = new ArrayData(array(
            'Date' => $date
        ));
        return array();
    }
    
    private $_article;
    
    public function Article() {
        return $this->_article;
    }
    
    private $_month;
    
    public function Month() {
        return $this->_month;
    }
    
    private $_articles;
    
    public function FilteredArticles() {
        if (is_null($this->_articles)) {
            $this->_articles = $this->_Articles();
            if (!is_null($this->_month)) {
                $date = strtotime($this->_month->Date->getValue());
                $end = new SS_Datetime();
                $end->setValue(mktime(0, 0, 0, date('m', $date) + 1, date('d', $date), date('Y', $date)));
                $this->_articles = $this->_articles->filter(array(
                    'Date:GreaterThanOrEqual' => $this->_month->Date->Rfc3339(),
                    'Date:LessThan' => $end->Rfc3339()
                ));
            }
        }
        return $this->_articles;
    }
    
    public function PagedArticles() {
        $pages = new PaginatedList($this->FilteredArticles(), $this->request);
        $pages->setPageLength($this->ArticlesPerPage);
        return $pages;
    }
    
    public function Crumbs($maxDepth = 20, $unlinked = false, $stopAtPageType = false, $showHidden = false) {
        $crumbs = parent::Crumbs($maxDepth, $unlinked, $stopAtPageType, $showHidden);
        if (!is_null($this->_article)) {
            $last = $crumbs->last();
            $cls = get_class($last);
            $article = new $cls($last->toMap());
            $article->MenuTitle = $this->_article->Title;
            $crumbs->push($article);
        }
        if (!is_null($this->_month)) {
            $last = $crumbs->last();
            $cls = get_class($last);
            $month = new $cls($last->toMap());
            $month->MenuTitle = $this->_month->Date->Format('F Y');
            $crumbs->push($month);
        }
        return $crumbs;
    }
}