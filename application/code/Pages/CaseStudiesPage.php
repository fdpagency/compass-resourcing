<?php

class CaseStudiesPage extends Page {

    public static $db = array(
    );

    public static $has_one = array(
    );

    public static $has_many = array(
        'CaseStudies' => 'CaseStudy'
    );

    function getCMSFields() {
        $fields = parent::getCMSFields();

        $gridConfig = GridFieldConfig::create()->addComponents(
            new GridFieldToolbarHeader(),
            new GridFieldAddNewButton('toolbar-header-right'),
            new GridFieldSortableHeader(),
            new GridFieldDataColumns(),
            new GridFieldPaginator(20),
            new GridFieldEditButton(),
            new GridFieldDeleteAction(),
            new GridFieldDetailForm(),
            new GridFieldSortableRows('SortOrder')
        );

        $gridField = new GridField("CaseStudies", "Case Studies", $this->CaseStudies(), $gridConfig);
        $fields->addFieldToTab('Root.Case Studies', $gridField);

        return $fields;
    }

}
class CaseStudiesPage_Controller extends Page_Controller {

    public static $allowed_actions = array (
    );

    public function init() {
        parent::init();
    }

}