<?php


class CVPage extends Page {
    
    private static $db = array(
        'Recipients' => 'Text',
    	'ThankYouMessage' => 'HTMLText'
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
    	$fields->addFieldToTab(
    	    'Root.Form', 
    	    TextField::create('Recipients', 'Recipients of CV Emails (comma separated list)')
        );
    	$fields->addFieldToTab(
            'Root.Form', 
            HtmlEditorField::create('ThankYouMessage', 'Thank You Message')
        );
        return $fields;
    }
}


class CVPage_Controller extends Page_Controller {
    
    private static $allowed_actions = array(
        'CVForm'
    );
    
	public function CVForm() {
        return CVForm::create($this);
    }
    
    public function SubmitCV($data, $form) {
		$email = CVEmail::create(
		    new ArrayData($data), 
		    $this->Recipients
        );
		$email->send();
        $this->redirect($this->Link('?sent=1'));
    }
    
    public function Sent() {
        return array_key_exists('sent', $_GET);
    }
}