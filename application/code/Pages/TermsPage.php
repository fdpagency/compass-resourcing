<?php


class TermsPage extends Page {
	
	private static $db = array(
        'Recipients' => 'Text',
    	'ThankYouMessage' => 'HTMLText'
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
    	$fields->addFieldToTab(
    	    'Root.Form', 
    	    TextField::create('Recipients', 'Recipients of Terms of Business Emails (comma separated list)')
        );
    	$fields->addFieldToTab(
            'Root.Form', 
            HtmlEditorField::create('ThankYouMessage', 'Thank You Message')
        );
        return $fields;
    }
}


class TermsPage_Controller extends Page_Controller {
	
	private static $allowed_actions = array(
		'TermsForm'
	);
	
	private $_terms;
	
	public function Terms() {
		if (is_null($this->_terms)) {
			$this->_terms = Terms::get()->first();
		}
		return $this->_terms;
	}
	
	public function TermsForm() {
		if ($member = Member::currentUser()) {
			if (!is_a($member, 'Member')) {
				$member = Member::get()->filter('ID', $member)->first();
			}
			if (!$member->termsUpToDate()) {
				return TermsForm::create($this);
			}
		}
		return null;
	}
	
	public function ApproveTerms($data, $form) {
		$member = $this->currentUser();
		$member->ApprovedTerms()->add($this->Terms(), array_merge($data, array('Date' => date('Y-m-d H:i:s'))));
		
		$approval = ArrayData::create($data);
		$email = TermsApprovedEmail::create(
			$this->Terms(),
			$approval,
			$this->Recipients
		);
		$email->send();
		
		$email = TermsApprovedThankYouEmail::create(
			$this->Terms(),
			$approval
		);
		$email->send();
		
		$this->redirectBack();
	}
}