<?php


class PermanentVacancyPage extends Page {
    
    private static $db = array(
        'Recipients' => 'Text',
    	'ThankYouMessage' => 'HTMLText'
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
    	$fields->addFieldToTab(
    	    'Root.Form', 
    	    TextField::create('Recipients', 'Recipients of Permanent Vacancy Emails (comma separated list)')
        );
    	$fields->addFieldToTab(
            'Root.Form', 
            HtmlEditorField::create('ThankYouMessage', 'Thank You Message')
        );
        return $fields;
    }
}


class PermanentVacancyPage_Controller extends Page_Controller {
    
    private static $allowed_actions = array(
        'PermanentVacancyForm'
    );
    
	public function PermanentVacancyForm() {
        return PermanentVacancyForm::create($this);
    }
    
    public function SubmitPermanentVacancy($data, $form) {
		$email = PermanentVacancyEmail::create(
		    new ArrayData($data), 
		    $this->Recipients
        );
		$email->send();
        $this->redirect($this->Link('?sent=1'));
    }
    
    public function Sent() {
        return array_key_exists('sent', $_GET);
    }
}