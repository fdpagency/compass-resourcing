<?php


class TimesheetPage extends Page {
    
    private static $db = array(
		'Introduction' => 'Text',
		'Warning' => 'Text',
		'Statements' => 'HTMLText',
        'CompassRecipients' => 'Varchar(200)',
		'CardinalRecipients' => 'Varchar(200)',
        'ThankYouMessage' => 'HTMLText'
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
		$fields->addFieldToTab(
			'Root.Main',
			TextareaField::create('Introduction', 'Introduction (appears on selection screen)'),
			'Content'
		);
		$fields->addFieldToTab(
			'Root.Main',
			TextareaField::create('Warning', 'Warning (appears in red box)'),
			'Content'
		);
		$fields->addFieldToTab(
			'Root.Main',
			HTMLEditorField::create('Statements', 'Candidate Statements'),
			'Content'
		);
		$fields->removeByName('Content');
        $fields->addFieldToTab(
    	    'Root.Form', 
    	    TextField::create('CompassRecipients', 'Recipients of compass timesheets (comma separated list)')
        );
		$fields->addFieldToTab(
    	    'Root.Form', 
    	    TextField::create('CardinalRecipients', 'Recipients of cardinal timesheets (comma separated list)')
        );
    	$fields->addFieldToTab(
    	    'Root.Form', 
    	    HtmlEditorField::create('ThankYouMessage', 'Thank You Message')
        );
        return $fields;
    }
}


class TimesheetPage_Controller extends Page_Controller {
    
    private static $allowed_actions = array(
        'Timesheet',
		'TimesheetStatementsForm',
        'TimesheetForm'
    );
    private static $url_handlers = array(
        'TimesheetStatementsForm' => 'TimesheetStatementsForm',
		'TimesheetForm' => 'TimesheetForm',
        '$Type!' => 'Timesheet'
    );
    
    public function init() {
        parent::init();
        if ($this->request->isPOST()) {
            $this->_setTimesheet($this->request->postVars());
        }
    }
    
    private $_timesheet;
    
    private function _setTimesheet($data) {
        if (array_key_exists('Type', $data) && !empty($data['Type'])) {
            foreach ($this->Types() as $type) {
                if ($type->Name == $data['Type'] || $type->Slug == $data['Type']) {
                    $this->_timesheet = Timesheet::create(array(
                        'Type' => $type->Name
                    ));
                }
            }
        }
    }
    
    private static $types = array(
        array(
            'Name' => 'Compass',
            'Title' => 'Timesheet',
            'Slug' => 'compass'
        ),
        array(
            'Name' => 'Cardinal',
            'Title' => 'Project Management Timesheet',
            'Slug' => 'cardinal'
        )
    );
    
    private $_type;
    
    public function Type() {
        if (is_null($this->_type) && !is_null($this->_timesheet)) {
            foreach ($this->Types() as $type) {
                if ($type->Name == $this->_timesheet->Type) {
                    $this->_type = $type;
                }
            }
        }
        return $this->_type;
    }
    
    private $_types;
    
    public function Types() {
        $this->_types = ArrayList::create();
        foreach ($this->stat('types') as $type => $attrs) {
            $this->_types->push(ArrayData::create(array_merge($attrs, array(
                'Link' => $this->Link($attrs['Slug'])
            ))));
        }
        return $this->_types;
    }
    
    public function Timesheet(SS_HTTPRequest $request) {
        $this->_setTimesheet($request->params());
		
        return array();
    }
    
    public function TimesheetStatementsForm() {
		if (!Session::get(sprintf('Statements%s', $this->Type()->Name)) == 1) {
			return TimesheetStatementsForm::create($this);
		}
		return null;
    }
	
	public function SubmitStatements($data, $form) {
		Session::set(sprintf('Statements%s', $this->Type()->Name), 1);
		$this->redirectBack();
	}
	
	public function TimesheetForm() {
        return TimesheetForm::create($this, $this->_timesheet, array(
			'SubmitTimesheet' => 'Submit'
		));
    }
    
    public function SubmitTimesheet($data, $form) {
		$timesheet = $this->_timesheet;
        $form->saveInto($timesheet);
        $timesheet->write();
		$recipients = sprintf('%sRecipients', $timesheet->Type);
        $email = TimesheetEmail::create($timesheet, $this->$recipients);
        $email->send();
        $email = TimesheetThankYouEmail::create($timesheet);
        $email->send();
		Session::clear(sprintf('Statements%s', $this->Type()->Name));
        $this->redirect(sprintf('%s/?sent=1', $this->Type()->Link));
    }
    
    public function Sent() {
        return array_key_exists('sent', $this->request->getVars());
    }
}