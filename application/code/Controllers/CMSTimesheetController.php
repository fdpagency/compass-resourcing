<?php


class CMSTimesheetController extends LeftAndMain implements PermissionProvider {
    
    private static $menu_title = 'Timesheets';
    private static $url_segment = 'timesheets';
	private static $url_rule = '/$Action/$ID/$OtherID';
    private static $menu_icon = 'application/images/CMSTimesheetController.png';
    
	private static $allowed_actions = array(
		'Index',
		'Cardinal',
		'CardinalForm',
		'Compass',
		'CompassForm'
    );
	
	private $_types;
	
	public function Types() {
		if (is_null($this->_types)) {
			$this->_types = ArrayList::create();
			foreach (array('Cardinal', 'Compass') as $type) {
				$this->_types->push(ArrayData::create(array(
					'Name' => $type,
					'Link' => $this->Link($type)
				)));
			}
		}
		return $this->_types;
	}
	
	private $_tab = 'Cardinal';
	
	public function Tab() {
		return $this->_tab;
	}
	
	public function init() {
		parent::init();
		if ($this->request->isPOST()) {
			foreach ($this->request->postVars() as $k => $v) {
				if (preg_match('/^(Cardinal|Compass|Clients)/', $k, $match)) {
					$this->_tab = $match[1];
					break;
				}
			}
		}
		elseif ($action = $this->request->param('Action')) {
			$this->_tab = $action;
		}
	}
	
	public function providePermissions() {
		return array(
			sprintf('CMS_ACCESS_%s', get_class($this)) => array(
				'name' => "Access to 'Timesheets' section",
				'category' => 'CMS Access'
			)
		);
	}
	
	public function Index($request) {
		return $this->_response($request);
	}
	
	public function Cardinal($request) {
		return $this->_response($request);
	}
	
	public function Compass($request) {
		return $this->_response($request);
	}
	
	private function _response($request) {
		if ($pjax = $request->getHeader('X-Pjax')) {
			return $this->renderWith(sprintf('CMSTimesheetController_%s', $pjax));
		}
		else {
			return array();
		}
	}
	
	private function _timesheetsFields($form, $type) {
		$pending_config = GridFieldConfig::create()->addComponents(
			new GridFieldToolbarHeader(),
			new GridFieldSortableHeader(),
			new GridFieldDataColumns(),
			new GridFieldPaginator(30),
			new GridFieldTimesheetSignOffSelector('Sign Off 1'),
			new GridFieldTimesheetSignOffSelector('Sign Off 2', true),
			new GridFieldTimesheetSendButton(array('SignOff1', 'SignOff2')),
			new GridFieldTimesheetDeleteSelectedButton('Select'),
			new GridFieldTimesheetDownloader(),
			new GridFieldEditButton(),
			new GridFieldDeleteAction(),
			new GridFieldDetailForm(),
			new GridFieldTimesheetSelector('Select')
		);
		$pending_config->getComponentByType('GridFieldDataColumns')->setDisplayFields(Timesheet::sign_off_fields());
		
		$single_config = GridFieldConfig::create()->addComponents(
			new GridFieldToolbarHeader(),
			new GridFieldSortableHeader(),
			new GridFieldDataColumns(),
			new GridFieldPaginator(30),
			new GridFieldTimesheetDeleteSelectedButton('Select'),
			new GridFieldTimesheetDownloader(),
			new GridFieldTimesheetReset(),
			new GridFieldEditButton(),
			new GridFieldDeleteAction(),
			new GridFieldDetailForm(),
			new GridFieldTimesheetSelector('Select')
		);
		$single_config->getComponentByType('GridFieldDataColumns')->setDisplayFields(Timesheet::single_sign_off_fields());
		
		$signed_off_config = GridFieldConfig::create()->addComponents(
			new GridFieldToolbarHeader(),
			new GridFieldSortableHeader(),
			new GridFieldDataColumns(),
			new GridFieldPaginator(30),
			new GridFieldTimesheetArchiveAllButton(),
			new GridFieldTimesheetExportButton($type),
			new GridFieldTimesheetDownloader(),
			new GridFieldTimesheetReset(),
			new GridFieldEditButton(),
			new GridFieldTimesheetArchive(),
			new GridFieldDetailForm()
		);
		$signed_off_config->getComponentByType('GridFieldDataColumns')->setDisplayFields(Timesheet::signed_off_fields());
		
		return FieldList::create(TabSet::create(
			'Main',
			Tab::create(
				'Pending',
				GridField::create(sprintf('%sPendingTimesheets', $type), 'Pending', Timesheet::get()->filter('Type', $type)->where('Archived IS FALSE AND Approved IS NULL AND SignOff1ID < 1'), $pending_config)->setForm($form)
			)->setTitle('Pending'),
			Tab::create(
				'AwaitingSingleSignOff',
				GridField::create(sprintf('%sAwaitingSingleSignOff', $type), 'Awaiting Sign Off (Single)', Timesheet::get()->filter('Type', $type)->where('Archived IS FALSE AND Approved IS NULL AND SignOff1ID > 0 AND SignOff2ID < 1'), $single_config)->setForm($form)
			)->setTitle('Awaiting Single Sign Off'),
			Tab::create(
				'AwaitingDoubleSignOff',
				GridField::create(sprintf('%sAwaitingDoubleSignOff', $type), 'Awaiting Sign Off (Double)', Timesheet::get()->filter('Type', $type)->where('Archived IS FALSE AND Approved IS NULL AND SignOff1ID > 0 AND SignOff2ID > 0'), GridFieldConfig::create()->addComponents(
					new GridFieldToolbarHeader(),
					new GridFieldSortableHeader(),
					new GridFieldDataColumns(),
					new GridFieldPaginator(30),
					new GridFieldTimesheetDeleteSelectedButton('Select'),
					new GridFieldTimesheetDownloader(),
					new GridFieldTimesheetReset(),
					new GridFieldEditButton(),
					new GridFieldDeleteAction(),
					new GridFieldDetailForm(),
					new GridFieldTimesheetSelector('Select')
				))->setForm($form)
			)->setTitle('Awaiting Double Sign Off'),
			Tab::create(
				'SignedOff',
				GridField::create(sprintf('%sSignedOffTimesheets', $type), 'Signed Off', Timesheet::get()->filter('Type', $type)->where('Archived IS FALSE AND Approved IS NOT NULL'), $signed_off_config)->setForm($form)
			)->setTitle('Signed Off'),
			Tab::create(
				'Archived',
				GridField::create(sprintf('%sArchived', $type), 'Archived', Timesheet::get()->filter('Type', $type)->where('Archived IS TRUE AND Approved IS NOT NULL'), GridFieldConfig::create()->addComponents(
					new GridFieldToolbarHeader(),
					new GridFieldSortableHeader(),
					new GridFieldDataColumns(),
					new GridFieldPaginator(30),
					new GridFieldTimesheetDownloader()
				))->setForm($form)
			)->setTitle('Archived')
		));
	}
	
	public function CardinalForm() {
		$form = $this->getEditForm();
		$form->setName('CardinalForm');
		$form->setFields($this->_timesheetsFields($form, 'Cardinal'));
		return $form;
	}
	
	public function CompassForm() {
		$form = $this->getEditForm();
		$form->setName('CompassForm');
		$form->setFields($this->_timesheetsFields($form, 'Compass'));
		return $form;
	}
	
	public function TabForm() {
		switch ($this->_tab) {
			case 'Cardinal':
				return $this->CardinalForm();
				break;
			case 'Compass':
				return $this->CompassForm();
				break;
			default:
				return null;
				break;
		}
	}
	
	private $_crumbs;
	
	public function Breadcrumbs($unlinked = false) {
		$crumbs = parent::Breadcrumbs($unlinked);
		$action = $this->request->param('Action');
		$id = $this->request->param('OtherID');
		if (!empty($action) && preg_match('/^(Cardinal|Compass|Clients).*?/', $action, $match)) {
			$crumbs->push(ArrayData::create(array(
				'Title' => $match[1],
				'Link' => sprintf('admin/%s/%s/', $this->stat('url_segment'), $match[1])
			)));
		}
		elseif (!empty($id) && preg_match('/^(Cardinal|Compass|Clients).*?/', $id, $match)) {
			$crumbs->push(ArrayData::create(array(
				'Title' => $match[1],
				'Link' => sprintf('admin/%s/%s/', $this->stat('url_segment'), $match[1])
			)));
		}
		return $crumbs;
	}
}


class GridFieldTimesheetSignOffSelector implements GridField_ColumnProvider {
    
    private $_title, $_field, $_include_status;
	
	public function __construct($title, $include_status = false) {
		$this->_title = $title;
        $this->_field = str_replace(' ', '', $title);
    }
	
	public function augmentColumns($grid, &$columns) {
        if (!in_array($this->_field, $columns)) {
            $columns[] = $this->_field;
        }
		if ($this->_include_status) {
			if (!in_array('Status', $columns)) {
				$columns[] = 'Status';
			}
		}
    }
    
    public function getColumnAttributes($grid, $timesheet, $column) {
        if ($column == $this->_field) {
            return array('class' => 'col-buttons col-dropdown');
        }
		if ($column == 'Status') {
			return array();
		}
    }
    
    public function getColumnMetadata($grid, $column) {
        if ($column == $this->_field) {
            return array('title' => $this->_title);
        }
        if ($column == 'Status') {
            return array('title' => 'Status');
        }
    }
    
    public function getColumnsHandled($grid) {
        $columns = array($this->_field);
		if ($this->_include_status) {
			$columns[] = 'Status';
		}
		return $columns;
    }
    
    public function getColumnContent($grid, $timesheet, $column) {
        Requirements::javascript(sprintf('%s/javascript/%s.js', project(), get_class($this)));
        switch ($column) {
            case $this->_field:
				$id_attr = sprintf('%sID', $this->_field);
                if ($timesheet->$id_attr < 1) {
                    $field = DropdownField::create(
                        sprintf('%sID%d', $this->_field, $timesheet->ID),
                        $this->_field,
                        Compass_Member::timesheet_map()
                    )->addExtraClass('no-change-track')->setEmptyString('----------');
                    return $field->Field();
                }
                else {
					$field_name = $this->_field;
                    return $timesheet->$field_name()->getNameWithClient();
                }
                break;
            case 'Status':
                return $timesheet->Status;
                break;
        }
    }
}

class GridFieldTimesheetSendButton implements GridField_HTMLProvider, GridField_ActionProvider {
    
    private $_fields, $_target;
    
    public function __construct($fields = array(), $target = 'toolbar-header-right') {
		$this->_fields = $fields;
        $this->_target = $target;
    }
    
    public function getHTMLFragments($grid) {
        $button = GridField_FormAction::create(
            $grid,
            'SendTimesheets',
            'Send',
            'SendTimesheets',
            null
        )->setAttribute('data-icon', 'drive-upload')->addExtraClass('action-send ss-ui-action-constructive');
        return array(
            $this->_target => $button->Field()
        );
    }
    
    public function getActions($grid) {
        return array('SendTimesheets');
    }
    
    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == 'sendtimesheets') {
            $members = array();
            foreach ($grid->getList() as $timesheet) {
				$sign_offs = array();
				foreach ($this->_fields as $field) {
					$id_attr = sprintf('%sID', $field);
					$sign_off_id = sprintf('%s%d', $id_attr, $timesheet->ID);
					if (array_key_exists($sign_off_id, $data) && $data[$sign_off_id] > 0) {
						if (!in_array($data[$sign_off_id], $sign_offs)) {
							$sign_offs[] = $data[$sign_off_id];
						}
					}
				}
				$i = 0;
				foreach ($sign_offs as $sign_off) {
					$field = $this->_fields[$i];
					$id_attr = sprintf('%sID', $field);
					$timesheet->$id_attr = $sign_off;
					$timesheet->write();
					if (!array_key_exists($timesheet->$id_attr, $members)) {
						$members[$timesheet->$id_attr] = array(
							'Member' => $timesheet->$field(),
							'Timesheets' => ArrayList::create()
						);
					}
					$members[$timesheet->$id_attr]['Timesheets']->push($timesheet);
					$i++;
				}
            }
            foreach ($members as $id => $attrs) {
                $email = TimesheetSignOffEmail::create($attrs['Timesheets'], $attrs['Member']);
                $email->send();
            }
            $grid->form->getController()->redirectBack();
        }
    }
}

class GridFieldTimesheetSelector implements GridField_ColumnProvider {
    
    private $_title, $_field;
	
	public function __construct($title) {
		$this->_title = $title;
        $this->_field = str_replace(' ', '', $title);
    }
	
	public function augmentColumns($grid, &$columns) {
        if (!in_array($this->_field, $columns)) {
            $columns[] = $this->_field;
        }
    }
    
    public function getColumnAttributes($grid, $timesheet, $column) {
        if ($column == $this->_field) {
            return array('class' => 'col-buttons col-dropdown col-centred');
        }
    }
    
    public function getColumnMetadata($grid, $column) {
        if ($column == $this->_field) {
            return array('title' => $this->_title);
        }
    }
    
    public function getColumnsHandled($grid) {
        return array($this->_field);
    }
    
    public function getColumnContent($grid, $timesheet, $column) {
        switch ($column) {
            case $this->_field:
				$field = CheckboxField::create(
					sprintf('%sID%d', $this->_field, $timesheet->ID),
					$this->_title
				)->addExtraClass('no-change-track');
                return $field->Field();
                break;
        }
    }
}

class GridFieldTimesheetDeleteSelectedButton implements GridField_HTMLProvider, GridField_ActionProvider {
    
    private $_field, $_target;
    
    public function __construct($field, $target = 'toolbar-header-right') {
		$this->_field = $field;
        $this->_target = $target;
    }
    
    public function getHTMLFragments($grid) {
		Requirements::javascript(sprintf('%s/javascript/%s.js', project(), get_class($this)));
        $button = GridField_FormAction::create(
            $grid,
            'DeleteTimesheets',
            'Delete Selected',
            'DeleteTimesheets',
            null
        )->addExtraClass('button-delete');
        return array(
            $this->_target => $button->Field()
        );
    }
    
    public function getActions($grid) {
        return array('DeleteTimesheets');
    }
    
    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == 'deletetimesheets') {
			$delete = array();
            foreach ($grid->getList() as $timesheet) {
				$selected = sprintf('%sID%d', $this->_field, $timesheet->ID);
				if (array_key_exists($selected, $data) && $data[$selected]) {
					$timesheet->delete();
				}
            }
            $grid->form->getController()->redirectBack();
        }
    }
}

class GridFieldTimesheetExportButton implements GridField_HTMLProvider, GridField_ActionProvider {
    
    private $_type, $_target;
    
    public function __construct($type, $target = 'toolbar-header-right') {
		$this->_type = $type;
        $this->_target = $target;
    }
    
    public function getHTMLFragments($grid) {
        $button = GridField_FormAction::create(
            $grid,
            'ExportTimesheets',
            'Export to CSV',
            'ExportTimesheets',
            null
        )->setAttribute('data-icon', 'download-csv')->addExtraClass('no-ajax');
        return array(
            $this->_target => $button->Field()
        );
    }
    
    public function getActions($grid) {
        return array('ExportTimesheets');
    }
    
    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == 'exporttimesheets') {
			$day_columns = array(
				'Job Description',
				'Job Number / Purchase Order',
				'Start Time',
				'Finish Time',
				'Breaks',
				'Hours Days',
				'Hours Nights',
			);
			$day_headings = array();
			$days = Timesheet::line_days($this->_type);
			foreach ($days as $day => $seconds) {
				foreach ($day_columns as $column) {
					$day_headings[] = sprintf('%s - %s', date('l', $seconds), $column);
				}
			}
			ob_start();
			$output = fopen('php://output', 'w');
			fputcsv($output, array_merge(
				array(
					'Company Name',
					'Weekending Date',
					'Your Name (Candidate Name)',
					'Site Name'
				),
				$day_headings,
				array(
					'Total Breaks',
					'Total Hours Days',
					'Total Hours Nights',
					'Notes',
					'Signed off by 1',
					'Date',
					'Signed off by 2',
					'Date'
				)
			));
			
            foreach ($grid->getList() as $timesheet) {
				$lines = array();
				foreach ($days as $day => $seconds) {
					for ($i = 0; $i < count($day_columns); $i++) {
						$lines[] = '';
					}
				}
				foreach ($timesheet->Lines() as $line) {
					$index = array_search(date('l', strtotime($line->Date)), array_keys($days)) * count($day_columns);
					array_splice($lines, $index, count($day_columns), array(
						'',
						$line->JobNumber,
						date('H:i', strtotime($line->Start)),
						date('H:i', strtotime($line->End)),
						$line->Breaks,
						$line->HoursDays,
						$line->HoursNights
					));
				}
				fputcsv($output, array_merge(
					array(
						$timesheet->CompanyName,
						date('d/m/Y', strtotime($timesheet->EndDate)),
						$timesheet->Name,
						$timesheet->Location
					),
					$lines,
					array(
						$timesheet->getTotalBreaks(),
						$timesheet->getTotalHoursDays(),
						$timesheet->getTotalHoursNights(),
						$timesheet->CandidateNotes,
						($timesheet->SignOff1ID > 0) ? $timesheet->SignOff1()->getNameWithClient() : '',
						($timesheet->SignOff1ID > 0) ? date('d/m/Y', strtotime($timesheet->SignedOff1)) : '',
						($timesheet->SignOff2ID > 0) ? $timesheet->SignOff2()->getNameWithClient() : '',
						($timesheet->SignOff2ID > 0) ? date('d/m/Y', strtotime($timesheet->SignedOff2)) : ''
					)
				));
            }
			$data = ob_get_clean();
			return SS_HTTPRequest::send_file(
				$data,
				sprintf('Exported%sTimesheets-%s.csv', $this->_type, date('dmY')),
				'text/csv'
			);
        }
    }
}

class GridFieldTimesheetArchiveAllButton implements GridField_HTMLProvider, GridField_ActionProvider {
    
    private $_fields, $_target;
    
    public function __construct($target = 'toolbar-header-right') {
        $this->_target = $target;
    }
    
    public function getHTMLFragments($grid) {
		Requirements::javascript(sprintf('%s/javascript/%s.js', project(), get_class($this)));
        $button = GridField_FormAction::create(
            $grid,
            'ArchiveTimesheets',
            'Archive All',
            'ArchiveTimesheets',
            null
        )->addExtraClass('button-delete');
        return array(
            $this->_target => $button->Field()
        );
    }
    
    public function getActions($grid) {
        return array('ArchiveTimesheets');
    }
    
    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == 'archivetimesheets') {
            foreach ($grid->getList() as $timesheet) {
				$timesheet->Archived = true;
				$timesheet->write();
            }
            $grid->form->getController()->redirectBack();
        }
    }
}


class GridFieldTimesheetDownloader implements GridField_ColumnProvider, GridField_ActionProvider {
    
    public function augmentColumns($grid, &$columns) {
        if (!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }
    
    public function getColumnAttributes($grid, $record, $column) {
        return array('class' => 'col-buttons');
    }
    
    public function getColumnMetadata($grid, $column) {
        if ($column == 'Actions') {
            return array('title' => '');
        }
    }
    
    public function getColumnsHandled($grid) {
        return array('Actions');
    }
    
    public function getColumnContent($grid, $record, $column) {
        $field = GridField_FormAction::create(
            $grid,
            'DownloadTimesheet' . $record->ID,
            'Download',
            'DownloadTimesheet',
            array('ID' => $record->ID)
        )->setAttribute('data-icon', 'disk')->addExtraClass('no-ajax');
        return $field->Field();
    }
    
    public function getActions($grid) {
        return array('DownloadTimesheet');
    }
    
    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == 'downloadtimesheet') {
            $timesheet = $grid->getList()->filter('ID', $args['ID'])->first();
            if (!is_null($timesheet)) {
                return SS_HTTPRequest::send_file(
					$timesheet->output(),
					sprintf(
						'%s-%s.xlsx',
						preg_replace('/[^a-zA-Z0-9]*/', '', $timesheet->Name),
						date('dmY', strtotime($timesheet->EndDate))
					),
					'application/vnd.ms-excel'
				);
            }
        }
    }
}


class GridFieldTimesheetReset implements GridField_ColumnProvider, GridField_ActionProvider {
    
    public function augmentColumns($grid, &$columns) {
        if (!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }
    
    public function getColumnAttributes($grid, $record, $column) {
        return array('class' => 'col-buttons');
    }
    
    public function getColumnMetadata($grid, $column) {
        if ($column == 'Actions') {
            return array('title' => '');
        }
    }
    
    public function getColumnsHandled($grid) {
        return array('Actions');
    }
    
    public function getColumnContent($grid, $record, $column) {
        $field = GridField_FormAction::create(
            $grid,
            'ResetTimesheet' . $record->ID,
            'Reset',
            'ResetTimesheet',
            array('ID' => $record->ID)
        )->setAttribute('data-icon', 'arrow-circle-135-left');
        return $field->Field();
    }
    
    public function getActions($grid) {
        return array('ResetTimesheet');
    }
    
    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == 'resettimesheet') {
            $timesheet = $grid->getList()->filter('ID', $args['ID'])->first();
            if (!is_null($timesheet)) {
                $timesheet->SignOff1ID = 0;
				$timesheet->SignedOff1 = null;
				$timesheet->SignOff2ID = 0;
				$timesheet->SignedOff2 = null;
				$timesheet->Approved = null;
				$timesheet->Status = 'Pending';
				$timesheet->write();
            }
        }
		$grid->form->getController()->redirectBack();
    }
}


class GridFieldTimesheetArchive implements GridField_ColumnProvider, GridField_ActionProvider {
    
    public function augmentColumns($grid, &$columns) {
        if (!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }
    
    public function getColumnAttributes($grid, $record, $column) {
        return array('class' => 'col-buttons');
    }
    
    public function getColumnMetadata($grid, $column) {
        if ($column == 'Actions') {
            return array('title' => '');
        }
    }
    
    public function getColumnsHandled($grid) {
        return array('Actions');
    }
    
    public function getColumnContent($grid, $record, $column) {
        $field = GridField_FormAction::create(
            $grid,
            'ArchiveTimesheet' . $record->ID,
            'Archive',
            'ArchiveTimesheet',
            array('ID' => $record->ID)
        )->setAttribute('data-icon', 'chain-unchain');
        return $field->Field();
    }
    
    public function getActions($grid) {
        return array('ArchiveTimesheet');
    }
    
    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == 'archivetimesheet') {
            $timesheet = $grid->getList()->filter('ID', $args['ID'])->first();
            if (!is_null($timesheet)) {
                $timesheet->Archived = true;
				$timesheet->write();
            }
        }
		$grid->form->getController()->redirectBack();
    }
}