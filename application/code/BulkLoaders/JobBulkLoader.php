<?php


class JobBulkLoader extends ExportableBulkLoader {
    
	public $columnMap = array(
		'Reference' => 'Reference',
		'Title' => 'Title',
		'Description' => 'Description',
		'Industry' => '->setHasOneRelation',
		'Job Type' => 'JobType',
		'Employment Type' => 'EmploymentType',
		'Salary From' => 'MinPay',
		'Salary To' => 'MaxPay',
		'Period' => 'PaySchedule',
		'Address Line 1' => 'Address1',
		'Address Line 2' => 'Address2',
		'Town/City' => 'Town',
		'County' => 'County',
		'Postcode' => 'Postcode',
		'Expires' => 'Expires',
		'Keywords' => 'Keywords',
		'Manager' => '->setHasOneRelation',
		'Published?' => 'Published'
    );
	public $duplicateChecks = array(
		'Reference' => 'Reference'
	);
    
    private static $has_one_classes = array(
        'Industry' => array(
            'class_name' => 'Industry',
            'foreign_key' => 'IndustryID',
            'value_attr' => 'Name',
			'create' => true
        ),
		'Manager' => array(
			'class_name' => 'Member',
			'foreign_key' => 'ManagerID',
			'value_attr' => 'Email'
		)
    );
}