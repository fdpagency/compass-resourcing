<?php


class Compass_Member extends DataExtension {
    
    private static $has_one = array(
        'Client' => 'Client'
    );
    private static $has_many = array(
        'SignOff1Timesheets' => 'Timesheet.SignOff1',
		'SignOff2Timesheets' => 'Timesheet.SignOff2'
    );
	private static $many_many = array(
		'ApprovedTerms' => 'Terms'
	);
	private static $many_many_extraFields = array(
		'ApprovedTerms' => array(
			'Approved' => 'Boolean',
			'FirstName' => 'Varchar(200)',
			'MiddleName' => 'Varchar(200)',
			'Surname' => 'Varchar(200)',
			'DOB' => 'Date',
			'JobTitle' => 'Varchar(200)',
			'Company' => 'Varchar(200)',
			'Today' => 'Date',
			'Email' => 'Varchar(200)',
			'Date' => 'SS_Datetime'
		)
	);
    
    private static $casting = array(
        'NameWithClient' => 'Varchar'
    );
    
    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldToTab(
            'Root.Main',
            $fields->fieldByName('Root.Main.ClientID'),
            'LastVisited'
        );
		$fields->removeByName('SignOff1Timesheets');
		$fields->removeByName('SignOff2Timesheets');
		$fields->removeByName('ApprovedTerms');
		if ($this->owner->ID > 0) {
			$terms_columns = new GridFieldDataColumns();
			$terms_columns->setDisplayFields(array(
				'Version' => 'Version',
				'Created.Nice' => 'Date Issued',
				'DateAgreed.Nice' => 'Date Agreed'
			));
			$fields->addFieldToTab(
				'Root.Terms',
				GridField::create('Terms', 'Terms of Business', $this->owner->ApprovedTerms(), GridFieldConfig::create()->addComponents(
					new GridFieldToolbarHeader(),
					new GridFieldAddNewButton('toolbar-header-right'),
					new GridFieldSortableHeader(),
					$terms_columns,
					new GridFieldEditButton(),
					new GridFieldDeleteAction(),
					new GridFieldDetailForm()
				))
			);
		}
    }
    
    private static $_timesheet_map;
    
    public static function timesheet_map() {
        if (is_null(self::$_timesheet_map)) {
            $members = Member::get()->innerJoin(
                'Client', 'Client.ID = Member.ClientID'
            )->sort('Client.Name ASC, Surname ASC, FirstName ASC');
            self::$_timesheet_map = array();
            foreach ($members as $member) {
                self::$_timesheet_map[$member->ID] = $member->getNameWithClient();
            }
        }
        return self::$_timesheet_map;
    }
    
    public function getNameWithClient() {
        if ($this->owner->ID > 0) {
            return sprintf('%s (%s)', $this->owner->Name, $this->owner->Client()->Name);
        }
        else {
            return '';
        }
    }
    
    private $_pending_timesheets;
    
    public function PendingTimesheets() {
        if (is_null($this->_pending_timesheets)) {
            $this->_pending_timesheets = ArrayList::create();
			foreach ($this->owner->SignOff1Timesheets()->where('SignedOff1 IS NULL') as $timesheet) {
				$this->_pending_timesheets->push($timesheet);
			}
			foreach ($this->owner->SignOff2Timesheets()->where('SignedOff2 IS NULL') as $timesheet) {
				$this->_pending_timesheets->push($timesheet);
			}
        }
        return $this->_pending_timesheets;
    }
	
	public function termsUpToDate() {
		return $this->owner->ApprovedTerms()->max('Version') == Terms::latest_version();
	}
	
	private $_terms_status;
	
	public function TermsStatus() {
		if (is_null($this->_terms_status)) {
			$terms = $this->owner->ApprovedTerms()->first();
			$this->_terms_status = DBField::create_field(
				'HTMLText',
				is_null($terms) ? 'No' : sprintf('Yes (Version %d)<br /><strong>%s</strong>', $terms->owner->Version, $this->owner->termsUpToDate() ? 'Up-to-date' : 'Out-of-date')
			);
		}
		return $this->_terms_status;
	}
	
	private $_terms_date;
	
	public function TermsDate() {
		if (is_null($this->_terms_date)) {
			if ($this->owner->ApprovedTerms()->count() < 1) {
				$this->_terms_date = '';
			}
			else {
				$this->_terms_date = DBField::create_field('SS_Datetime', $this->owner->ApprovedTerms()->first()->Date);
			}
		}
		return $this->_terms_date;
	}
	
	public function AgreedDOB() {
		return DBField::create_field('Date', $this->owner->DOB);
	}
	
	public function AgreedToday() {
		return DBField::create_field('Date', $this->owner->Today);
	}
	
	public function AgreedDate() {
		return DBField::create_field('SS_Datetime', $this->owner->Date);
	}
	
	private $_reset_password_link;
	
	public function ResetPasswordLink() {
		if (is_null($this->_reset_password_link)) {
			$token = $this->owner->generateAutologinTokenAndStoreHash();
			$this->_reset_password_link = Security::getPasswordResetLink($this->owner, $token);
		}
		return $this->_reset_password_link;
	}
}