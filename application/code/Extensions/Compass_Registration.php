<?php


class Compass_Registration extends DataExtension {
    
    private static $_registration;
    
    public static function retrieve($type) {
        if (is_null(self::$_registration)) {
            self::$_registration = singleton($type)->get()->filter('ID', Session::get(singleton($type)->stat('session_key')))->first();
            if (is_null(self::$_registration)) {
                self::$_registration = new $type();
            }
        }
        return self::$_registration;
    }
    
    private static $db = array(
        'Completed' => 'SS_Datetime'
    );
    
    private static $show_footer_notice;
    
    private static $sections = array();
    
    public function getSectionFields($controller) {
        $fields = array();
        $group = array();
        if (array_key_exists($this->owner->Section, $this->owner->stat('sections'))) {
            $section = $this->owner->stat('sections')[$this->owner->Section];
            if (array_key_exists('Fields', $section)) {
                foreach ($section['Fields'] as $attrs) {
                    $field = $this->_generateField($attrs, $controller);
                    if (!is_null($field)) {
                        $fields[] = $field;
                    }
                }
            }
            $fields[] = HiddenField::create('RegistrationType', 'RegistrationType', get_class($this->owner));
            $group[] = FieldGroup::create($fields)->setTitle($section['Title']);
        }
        return FieldList::create($group);
    }
    
    private function _generateField($attrs, $controller) {
        $options = array();
        if (array_key_exists('Options', $attrs)) {
            if (is_array($attrs['Options'])) {
                $options = $attrs['Options'];
            }
            else {
                if ($attrs['Options'] == 'Enum') {
                    $options = singleton(get_class($this->owner))->dbObject($attrs['Name'])->enumValues();
                    if (array_key_exists('', $options)) {
                        unset($options['']);
                    }
                }
                else {
                    $options = singleton($attrs['Options'])->form_map();
                }
            }
        }
        $field = null;
        switch ($attrs['Type']) {
            case 'Text':
                $field = TextField::create($attrs['Name'], $attrs['Label']);
                break;
            case 'Textarea':
                $field = TextareaField::create($attrs['Name'], $attrs['Label'])->setRows(10);
                break;
            case 'Date':
                $field = DateField::create($attrs['Name'], $attrs['Label'])->addExtraClass('date');
                break;
            case 'Dropdown':
                $field = DropdownField::create($attrs['Name'], $attrs['Label'], $options)->setEmptyString(
                    array_key_exists('EmptyString', $attrs) ? $attrs['EmptyString'] : null
                );
                break;
            case 'OptionSet':
                $field = OptionSetField::create($attrs['Name'], $attrs['Label'], $options);
                break;
            case 'YesNo':
                $field = OptionSetField::create($attrs['Name'], $attrs['Label'], array('0' => 'No', '1' => 'Yes'));
                break;
            case 'Checkbox':
                $field = CheckboxField::create($attrs['Name'], $attrs['Label']);
                break;
            case 'Group':
                $children = array();
                if (array_key_exists('Fields', $attrs)) {
                    foreach ($attrs['Fields'] as $child_attrs) {
                        $child = $this->_generateField($child_attrs, $controller);
                        if (!is_null($child)) {
                            $children[] = $child;
                        }
                    }
                }
                $field = FieldGroup::create($children);
                if (array_key_exists('Title', $attrs)) {
                    $field->setTitle($attrs['Title']);
                }
                break;
            case 'Literal':
                $field = LiteralField::create($attrs['Name'], $attrs['Content']);
                break;
            case 'PageContent':
                $page_attr = $attrs['Attribute'];
                $content = $controller->$page_attr;
                $content = str_replace(
                    array('$Date', '$PensionDate', '$Name'), 
                    array(date('d/m/Y'), date('d/m/Y', strtotime('+3 months')), $this->owner->FullName()), 
                    $content
                );
                $field = LiteralField::create($attrs['Name'], $content);
                break;
            case 'Include':
                $field = LiteralField::create($attrs['Name'], $this->owner->renderWith(array($attrs['Template'])));
                break;
            case 'File':
                $field = FileField::create($attrs['Name'], $attrs['Label']);
                break;
            case 'RoleGroup':
                $categories = array();
                $responsibilities = array();
                foreach ($attrs['Categories'] as $category => $roles) {
                    $options = array(
                        '' => 'Please select if applicable'
                    );
                    foreach ($roles['Roles'] as $role) {
                        $options[$role['Name']] = $role['Name'];
                        if (array_key_exists('Responsibilities', $role)) {
                            foreach ($role['Responsibilities'] as $responsibility) {
                                if (!array_key_exists($responsibility, $responsibilities)) {
                                    $responsibilities[$responsibility] = array();
                                }
                                $responsibilities[$responsibility][] = $role['Name'];
                            }
                        }
                    }
                    $categories[] = OptionSetField::create(
                        sprintf('%s%s', $attrs['Name'], $category), 
                        sprintf('%s (Category %s)?', $attrs['Label'], $category),
                        $options
                    )->addExtraClass('role_category');
                }
                $responsibility_options = array();
                foreach ($responsibilities as $responsibility => $roles) {
                    $responsibility_options[$responsibility] = ArrayData::create(array(
                        'Name' => $responsibility,
                        'Roles' => implode(',', $roles)
                    ));
                }
                
                $field = FieldGroup::create(array_merge(
                    $categories, array(
                        CheckboxSetField::create(
                            sprintf('%sResponsibilities', $attrs['Name']), 
                            $attrs['Responsibilities']['Label'],
                            $responsibility_options
                        )->setTemplate('ResponsibilitiesCheckboxSetField')
                    )
                ));
                break;
        }
        if (!is_null($field)) {
            if (array_key_exists('ExtraClasses', $attrs)) {
                $field->addExtraClass($attrs['ExtraClasses']);
            }
        }
        return $field;
    }
    
    public function getSectionActions() {
        $actions = array();
        if (array_key_exists($this->owner->Section, $this->owner->stat('sections'))) {
            $section = $this->owner->stat('sections')[$this->owner->Section];
            if (array_key_exists('Actions', $section)) {
                foreach ($section['Actions'] as $attrs) {
                    $action = FormAction::create($attrs['Name'], $attrs['Label']);
                    if (array_key_exists('ExtraClasses', $attrs)) {
                        $action->addExtraClass($attrs['ExtraClasses']);
                    }
                    $actions[] = $action;
                }
            }
            else {
                $sections = array_values($this->owner->stat('sections'));
                if (array_search($section, $sections) != count($sections) -1) {
                    $actions[] = FormAction::create('PreviousSection', 'Back')->addExtraClass('back');
                    $actions[] = FormAction::create('NextSection', 'Next')->addExtraClass('next');
                }
            }
        }
        return new FieldList($actions);
    }
    
    public function getSectionValidator() {
        $validators = array();
        if (array_key_exists($this->owner->Section, $this->owner->stat('sections'))) {
            $section = $this->owner->stat('sections')[$this->owner->Section];
            if (array_key_exists('Fields', $section)) {
                foreach ($section['Fields'] as $field) {
                    $validators = array_merge($validators, $this->_getFieldValidators($field));
                }
            }
        }
        return new FormValidator($validators);
    }
    
    private function _getFieldValidators($attrs) {
        $validators = array();
        if ($attrs['Type'] == 'Group') {
            foreach ($attrs['Fields'] as $field) {
                $validators = array_merge($validators, $this->_getFieldValidators($field));
            }
        }
        else if ($attrs['Type'] == 'RoleGroup') {
            foreach ($attrs['Categories'] as $category => $roles) {
                if (array_key_exists('Validators', $roles)) {
                    $validators[sprintf('%s%s', $attrs['Name'], $category)] = $roles['Validators'];
                }
            }
            $validators[sprintf('%sResponsibilities', $attrs['Name'])] = $attrs['Responsibilities']['Validators'];
        }
        else if (array_key_exists('Validators', $attrs) && is_array($attrs['Validators'])) {
            $validators[$attrs['Name']] = $attrs['Validators'];
        }
        return $validators;
    }
    
    public function getSectionData() {
        $data = array();
        if (array_key_exists($this->owner->Section, $this->owner->stat('sections'))) {
            $section = $this->owner->stat('sections')[$this->owner->Section];
            if (array_key_exists('Fields', $section)) {
                foreach ($section['Fields'] as $field) {
                    if (array_key_exists('Default', $field)) {
                        if (array_key_exists('Options', $field) && is_string($field['Options'])) {
                            $default = singleton($field['Options'])->get()->filter('Default', true)->first();
                            if (!is_null($default)) {
                                $data[$field['Name']] = $default->ID;
                            }
                        }
                        else {
                            $data[$field['Name']] = $field['Default'];
                        }
                    }
                }
            }
        }
        return $data;
    }
    
    public function onAfterWrite() {
        Session::set($this->owner->stat('session_key'), $this->owner->ID);
    }
    
    public function nextSection() {
        $sections = array_keys($this->owner->stat('sections'));
        $current = array_search($this->owner->Section, $sections);
        $next = null;
        for ($i = $current + 1; $i < count($sections); $i++) {
            $section = $this->owner->stat('sections')[$sections[$i]];
            if (array_key_exists('Types', $section)) {
                if (in_array($this->owner->Type, $section['Types'])) {
                    $next = $i;
                    break;
                }
            }
            else {
                $next = $i;
                break;
            }
        }
        if (is_null($next)) {
            $this->owner->Completed = date('Y-m-d H:i:s');
            $this->owner->Section = 'Complete';
        }
        else {
            $this->owner->Section = $sections[$next];
        }
        $this->owner->write();
    }
    
    public function previousSection() {
        $sections = array_keys($this->owner->stat('sections'));
        $current = array_search($this->owner->Section, $sections);
        $previous = null;
        for ($i = $current - 1; $i >= 0; $i--) {
            $section = $this->owner->stat('sections')[$sections[$i]];
            if (array_key_exists('Types', $section)) {
                if (in_array($this->owner->Type, $section['Types'])) {
                    $previous = $i;
                    break;
                }
            }
            else {
                $previous = $i;
                break;
            }
        }
        if (!is_null($previous)) {
            $this->owner->Section = $sections[$previous];
        }
        $this->owner->write();
    }
    
    public function complete() {
        $this->owner->Completed = date('Y-m-d H:i:s');
        $this->owner->write();
        $this->owner->detach();
    }
    
    public function detach() {
        Session::clear($this->owner->stat('session_key'));
    }
    
    public function onBeforeDelete() {
        $this->owner->detach();
    }
    
    private $_output;
    
    public function Output() {
        if (is_null($this->_output)) {
            $sections = ArrayList::create();
            foreach ($this->owner->stat('sections') as $section) {
                if ((array_key_exists('Output', $section) && !$section['Output']) || (array_key_exists('Types', $section) && !in_array($this->owner->Type, $section['Types']))) {
                    continue;
                }
                $output_section = ArrayData::create(array(
                    'Title' => $section['Title'],
                    'Questions' => ArrayList::create()
                ));
                if (array_key_exists('Fields', $section)) {
                    foreach ($section['Fields'] as $field) {
                        $this->_generateFieldOutput($field, $output_section);
                    }
                }
                $sections->push($output_section);
            }
            $this->_output = ArrayData::create(array(
                'Title' => $this->owner->OutputTitle(),
                'Sections' => $sections
            ));
        }
        return $this->_output;
    }
    
    private function _generateFieldOutput($field, $output) {
        $name = array_key_exists('Name', $field) ? $field['Name'] : '';
        $options = array();
        $model_options = false;
        if (array_key_exists('Options', $field)) {
            if (is_array($field['Options'])) {
                $options = $field['Options'];
            }
            else {
                if ($field['Options'] == 'Enum') {
                    $options = singleton(get_class($this->owner))->dbObject($field['Name'])->enumValues();
                    if (array_key_exists('', $options)) {
                        unset($options['']);
                    }
                }
                else {
                    $options = singleton($field['Options'])->form_map();
                    $model_options = true;
                }
            }
        }
        else if ($field['Type'] == 'YesNo') {
            $options = array('0' => 'No', '1' => 'Yes');
        }
        switch ($field['Type']) {
            case 'Text':
            case 'Textarea':
                $output->Questions->push(ArrayData::create(array(
                    'Label' => $field['Label'],
                    'Value' => $this->owner->$name
                )));
                break;
            case 'Date':
                $output->Questions->push(ArrayData::create(array(
                    'Label' => $field['Label'],
                    'Value' => empty($this->owner->$name) ? '' : date('d/m/Y', strtotime($this->owner->$name))
                )));
                break;
            case 'Dropdown':
            case 'OptionSet':
            case 'YesNo':
                $output_options = ArrayList::create();
                foreach ($options as $value => $label) {
                    if (($model_options && $this->owner->$name == $value) || !$model_options) {
                        $output_options->push(ArrayData::create(array(
                            'Label' => $label,
                            'Selected' => $this->owner->$name == $value
                        )));
                    }
                }
                $output->Questions->push(ArrayData::create(array(
                    'Label' => $field['Label'],
                    'Options' => $output_options
                )));
                break;
            case 'Checkbox':
                $output->Questions->push(ArrayData::create(array(
                    'Label' => $field['Label'],
                    'Value' => $this->owner->$name == 1 ? 'Yes' : 'No'
                )));
                break;
            case 'Group':
                if (array_key_exists('Title', $field)) {
                    $output->Questions->push(ArrayData::create(array(
                        'GroupTitle' => $field['Title']
                    )));
                }
                foreach ($field['Fields'] as $child) {
                    $this->_generateFieldOutput($child, $output);
                }
                break;
            case 'RoleGroup':
                foreach ($field['Categories'] as $category => $roles) {
                    $name = sprintf('%s%s', $field['Name'], $category);
                    $category_options = ArrayList::create();
                    foreach ($roles['Roles'] as $role) {
                        $category_options->push(ArrayData::create(array(
                            'Label' => $role['Name'],
                            'Selected' => $this->owner->$name == $role['Name']
                        )));
                    }
                    $output->Questions->push(ArrayData::create(array(
                        'Label' => sprintf('%s (Category %s)?', $field['Label'], $category),
                        'Options' => $category_options
                    )));
                }
                $name = sprintf('%sResponsibilities', $field['Name']);
                $responsibilities = ArrayList::create();
                foreach (explode(',', $this->owner->$name) as $responsibility) {
                    $responsibilities->push(ArrayData::create(array(
                        'Label' => str_replace('{comma}', ',', $responsibility),
                        'Selected' => true
                    )));
                }
                $output->Questions->push(ArrayData::create(array(
                    'Label' => $field['Responsibilities']['Label'],
                    'Options' => $responsibilities
                )));
                break;
        }
    }
    
    public function render() {
        Requirements::clear();
        $html = $this->owner->renderWith('Registration');
        return $html;
    }
    
    private $_pdf;
    
    public function pdf($stream = true) {
        if (is_null($this->_pdf)) {
            $this->_pdf = new Compass_DOMPDF($this->owner);
            $this->_pdf->load_html($this->owner->render());
            $this->_pdf->render();
        }
        if ($stream) {
            $this->_pdf->stream();
            exit();
        }
        else {
            return $this->_pdf;
        }
    }
    
    public function OutputTitle() {
        return '';
    }
    
    public function FullName() {
        return '';
    }
    
    public function ShowFooterNotice() {
        return $this->owner->stat('show_footer_notice');
    }
}