<?php


class Compass_DataList extends Extension {
    
    private $_view_models;
    
    public function ViewModels() {
        if (is_null($this->_view_models)) {
            $this->_view_models = new ArrayList();
            foreach ($this->owner as $item) {
                $cls = sprintf('%sViewModel', get_class($item));
                if (class_exists($cls)) {
                    $this->_view_models->push(new $cls($item));
                }
            }
        }
        return $this->_view_models;
    }
}