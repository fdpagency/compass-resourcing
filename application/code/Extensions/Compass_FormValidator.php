<?php


class Compass_FormValidator extends Extension {
    
    public function telephone($value, $valid, $field) {
        if ($valid) {
            if (strlen(trim($value)) > 0) {
                return preg_match('/^(\+[0-9]{1})?[0-9]{11}$/', preg_replace('/[^0-9\+]*/', '', $value));
            }
        }
        return true;
    }
    
    public function postcode($value, $valid, $field) {
        if ($valid) {
            if (strlen(trim($value)) > 0) {
                return preg_match('/^[A-Z]{1,2}[0-9]{1,2}[A-Z]{0,1}[0-9]{1}[A-Z]{2}$/', preg_replace('/[^A-Z0-9]*/', '', strtoupper($value)));
            }
        }
        return true;
    }
    
    public function national_insurance($value, $valid, $field) {
        if ($valid) {
            if (strlen(trim($value)) > 0) {
                return preg_match('/^[A-Z]{2}[0-9]{6}[A-Z]{1}$/', preg_replace('/[^A-Z0-9]*/', '', strtoupper($value)));
            }
        }
        return true;
    }
    
    public function sort_code($value, $valid, $field) {
        if ($valid) {
            if (strlen(trim($value)) > 0) {
                return preg_match('/^[0-9]{2}\-[0-9]{2}\-[0-9]{2}$/', preg_replace('/[^0-9\-]*/', '', $value));
            }
        }
        return true;
    }
    
    public function account_number($value, $valid, $field) {
        if ($valid) {
            if (strlen(trim($value)) > 0) {
                return preg_match('/^[0-9]{8}$/', preg_replace('/[^0-9]*/', '', $value));
            }
        }
        return true;
    }
    
    public function utr_number($value, $valid, $field) {
        if ($valid) {
            if (strlen(trim($value)) > 0) {
                return preg_match('/^[0-9]{10}$/', $value);
            }
        }
        return true;
    }
    
    public function yes($value, $valid, $field) {
        if ($valid) {
            return trim($value) == '1';
        }
        return true;
    }
    
    public function no($value, $valid, $field) {
        if ($valid) {
            return trim($value) == '0';
        }
        return true;
    }
    
    public function required_null($value, $valid, $field) {
        return !$this->owner->required($value, $valid, $field);
    }
    
    public function required_upload_description($value, $valid, $field) {
        if (preg_match('/([0-9]*)$/', $field->getName(), $matches)) {
            $upload = $field->getForm()->Fields()->dataFieldByName(sprintf('Upload%d', $matches[1]));
            $file = $upload->Value();
            if (!$this->owner->required($field->Value(), true, $field) && is_array($file) && !empty($file['tmp_name'])) {
                return false;
            }
        }
        return true;
    }
    
    public function required_timesheet_line($value, $valid, $field) {
        if ($valid) {
            if (preg_match('/^([0-9]*)/', $field->getName(), $matches)) {
                if ($field->getForm()->Fields()->dataFieldByName(sprintf('%dDay', $matches[1]))->Value()) {
                    return $this->owner->required($value, $valid, $field);
                }
            }
        }
        return true;
    }
	
	public function timesheet_time($value, $valid, $field) {
		if ($valid) {
			if (preg_match('/^([0-9]*)/', $field->getName(), $matches)) {
                if ($field->getForm()->Fields()->dataFieldByName(sprintf('%dDay', $matches[1]))->Value()) {
					if (preg_match('/^([0-2]{1}[0-9]{1})\:([0-5]{1}[0-9]{1})$/', $value, $match)) {
						if ($match[1] > 23 || $match[2] > 59) {
							return false;
						}
						else {
							return true;
						}
					}
					else {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public function timesheet_time_end($value, $valid, $field) {
		if ($valid) {
			if (preg_match('/^([0-9]*)/', $field->getName(), $matches)) {
				if ($field->getForm()->Fields()->dataFieldByName(sprintf('%dDay', $matches[1]))->Value()) {
					if ($field->getForm()->Fields()->dataFieldByName(sprintf('%dDay', $matches[1]))->Value()) {
						$start = $field->getForm()->Fields()->dataFieldByName(sprintf('%dStart', $matches[1]))->Value();
						if ($this->timesheet_time($start, true, $field)) {
							$start_split = explode(':', $start);
							$end_split = explode(':', $value);
							return mktime($end_split[0], $end_split[1]) > mktime($start_split[0], $start_split[1]);
						}
					}
				}
            }
		}
		return true;
	}
}