<?php


class TimesheetForm extends Form {
    
    public static function create() {
        $args = func_get_args();
        $controller = $args[0];
        $timesheet = $args[1];
        $action_list = $args[2];
        
        $validators = array(
            'Name' => array(
                'required' => 'Please enter your name'
            ),
            'Email' => array(
                'required' => 'Please enter your email address',
                'email_address' => 'Please enter a valid email address'
            ),
            'CompanyName' => array(
                'required' => 'Please enter the name of the company'
            ),
			'Location' => array(
                'required' => 'Please enter the location where you worked'
            ),
			'JobTitle' => array(
                'required' => 'Please enter your job title'
            ),
            'EndDate' => array(
                'required' => 'Please select the week end date'
            )
        );
        
        $days = array();
        foreach ($timesheet->Days() as $day) {
            $iso = $day->Format('N');
            $days[] = FieldGroup::create(
                CheckboxField::create(sprintf('%sDay', $iso), $day->Format('l')),
                TextField::create(sprintf('%sJobNumber', $iso), 'Job Number'),
				TextField::create(sprintf('%sJobTitle', $iso), 'Job Title'),
                TimeField::create(sprintf('%sStart', $iso), 'Start Time<br />(24 hour e.g. 14:30)')->setConfig('timeformat', 'HH:mm')->setConfig('datavalueformat', 'HH:mm'),
                TimeField::create(sprintf('%sEnd', $iso), 'End Time<br />(24 hour e.g. 14:30)')->setConfig('timeformat', 'HH:mm')->setConfig('datavalueformat', 'HH:mm'),
                DropdownField::create(sprintf('%sBreaks', $iso), 'Breaks', array('0.5' => '30', '1' => '60', '1.5' => '90', '2' => '120'))->setEmptyString('Please select'),
                TextField::create(sprintf('%sHoursDays', $iso), 'Total Hours Days (minus breaks)'),
                TextField::create(sprintf('%sHoursNights', $iso), 'Total Hours Nights (minus breaks)')
            )->setTitle($day->Format('l'));
            $validators[sprintf('%sStart', $iso)] = array(
                'required_timesheet_line' => 'Error',
				'timesheet_time' => 'Error'
            );
            $validators[sprintf('%sEnd', $iso)] = array(
                'required_timesheet_line' => 'Error',
				'timesheet_time' => 'Error'
            );
            $validators[sprintf('%sBreaks', $iso)] = array(
                'required_timesheet_line' => 'Error'
            );
            $validators[sprintf('%sHoursDays', $iso)] = array(
                'required_timesheet_line' => 'Error'
            );
            $validators[sprintf('%sHoursNights', $iso)] = array(
                'required_timesheet_line' => 'Error'
            );
        }
		
        $dates = array();
        if ($timesheet->ID < 1) {
			$days = array_merge(array(FieldGroup::create(
                CheckboxField::create('ExampleDay', 'Example', true)->setDisabled(true)->setReadonly(true),
                TextField::create('ExampleJobNumber', 'Job Number', 'JOB001')->setDisabled(true)->setReadonly(true),
				TextField::create('ExampleJobTitle', 'Job Title', 'Joiner')->setDisabled(true)->setReadonly(true),
                TimeField::create('ExampleStart', 'Start Time<br />(24 hour e.g. 14:30)', '10:00')->setConfig('timeformat', 'HH:mm')->setConfig('datavalueformat', 'HH:mm')->setDisabled(true)->setReadonly(true),
                TimeField::create('ExampleEnd', 'End Time<br />(24 hour e.g. 14:30)', '16:00')->setConfig('timeformat', 'HH:mm')->setConfig('datavalueformat', 'HH:mm')->setDisabled(true)->setReadonly(true),
                DropdownField::create('ExampleBreaks', 'Breaks', array('0.5' => '30', '1' => '60', '1.5' => '90', '2' => '120'), '1')->setEmptyString('Please select')->setDisabled(true)->setReadonly(true),
                TextField::create('ExampleHoursDays', 'Total Hours Days (minus breaks)', '5')->setDisabled(true)->setReadonly(true),
                TextField::create('ExampleHoursNights', 'Total Hours Nights (minus breaks)', '0')->setDisabled(true)->setReadonly(true)
            )->setTitle('Example')->addExtraClass('example')), $days);
			
            $base = strtotime(sprintf('+%d days', $timesheet->Days()->last()->Format('N') - date('N', time())));
            for ($i = -2; $i <= 2; $i++) {
                $date = $base + ($i * 60 * 60 * 24 * 7);
                $dates[date('Y-m-d', $date)] = date('d/m/Y', $date);
            }
        }
        
        if ($timesheet->ID > 0) {
            $notes = FieldGroup::create(
                TextareaField::create('CandidateNotes', 'Candidate Notes'),
                TextareaField::create('ClientNotes', 'Client Notes')
            )->setName('Notes');
        }
        else {
            $notes = FieldGroup::create(
                TextareaField::create('CandidateNotes', 'Notes<br /><span>(e.g. Number of days accommodation?)</span>')
            )->setName('Notes');
        }
		
		$actions = array();
		if (!in_array($timesheet->Status, array('Signed Off', 'Archived'))) {
			foreach ($action_list as $name => $label) {
				$actions[] = FormAction::create($name, $label)->addExtraClass('submitButton alongside');
			}
		}
        
        $form = new TimesheetForm(
            $controller,
            'TimesheetForm',
            FieldList::create(array(
				FieldGroup::create(
					TextField::create('Name', 'Name'),
					TextField::create('Email', 'Email Address'),
					TextField::create('CompanyName', 'Company Name')->setReadonly($timesheet->Type == 'Cardinal'),
					(count($dates) > 0) ? DropdownField::create('EndDate', 'Week Ending', $dates)->setEmptyString('Please select') : TextField::create('EndDate', 'Week Ending')->setReadonly(true),
					TextField::create('Location', 'Site Worked')->addExtraClass('full')
				)->setName('Header'),
				FieldGroup::create($days)->setName('Days'),
				FieldGroup::create(
					TextField::create('HoursDays', 'Total Hours Days (minus breaks)'),
					TextField::create('HoursNights', 'Total Hours Nights (minus breaks)')
				)->setName('Footer'),
				$notes,
				HiddenField::create('Type', 'Type', $timesheet->Type),
				HiddenField::create('ID', 'ID', $timesheet->ID)
			)),
            FieldList::create(
                $actions
            ),
            new FormValidator($validators)
        );
        $form->loadDataFrom($timesheet, ($timesheet->ID < 1) ? Form::MERGE_IGNORE_FALSEISH : Form::MERGE_DEFAULT);
		
		if (in_array($timesheet->Status, array('Signed Off', 'Archived'))) {
			self::_disable_fields($form->Fields());
			$form->addExtraClass('readonly');
		}
        
        return $form;
    }
	
	private static function _disable_fields($list) {
		foreach ($list as $field) {
			if (is_subclass_of($field, 'CompositeField')) {
				self::_disable_fields($field->FieldList());
			}
			else {
				$field->setDisabled(true);
				$field->setReadonly(true);
			}
		}
	}
    
    public function forTemplate() {
    	Requirements::javascript(sprintf('%s/javascript/%s.js', $this->controller->ThemeDir(), get_class($this)));
    	return parent::forTemplate();
    }
    
    public function loadDataFrom($data, $merge = 0, $fields = null) {
        if (is_a($data, 'Timesheet')) {
            parent::loadDataFrom($data, $merge, array_keys(singleton('Timesheet')->stat('db')));
            if ($data->ID > 0) {
                parent::loadDataFrom(array('EndDate' => date('d/m/Y', strtotime($data->EndDate))), $merge);
            }
            if ($data->Type == 'Cardinal') {
                parent::loadDataFrom(array('CompanyName' => 'Cardinal Project Management'), $merge);
            }
            $line_fields = array_keys(singleton('TimesheetLine')->stat('db'));
            $lines = array();
            foreach ($data->Lines() as $line) {
                $iso = date('N', strtotime($line->Date));
                $lines[sprintf('%dDay', $iso)] = '1';
                foreach ($line_fields as $field) {
                    $lines[sprintf('%d%s', $iso, $field)] = $line->$field;
                }
            }
            parent::loadDataFrom($lines, $merge);
        }
        else {
            parent::loadDataFrom($data, $merge, $fields);
        }
		$example = array(
			'ExampleDay' => true,
			'ExampleJobNumber' => 'JOB001',
			'ExampleJobTitle' => 'Joiner',
			'ExampleStart' => '10:00',
			'ExampleEnd' => '16:00',
			'ExampleBreaks' => '1',
			'ExampleHoursDays' => 5,
			'ExampleHoursNights' => 0
		);
		parent::loadDataFrom($example, $merge, array_keys($example));
    }
    
    public function saveInto(DataObjectInterface $obj, $fields = null) {
        if (is_a($obj, 'Timesheet')) {
            parent::saveInto($obj, array_keys($obj->stat('db')));
            if ($obj->Type == 'Cardinal') {
                $obj->CompanyName = 'Cardinal Project Management';
            }
            $line_fields = array_keys(singleton('TimesheetLine')->stat('db'));
            $lines = $obj->Lines();
            $i = $obj->Days()->count() - 1;
            $data = $this->getData();
            foreach ($obj->Days() as $day) {
                $date = strtotime(sprintf('%s -%d days', $obj->EndDate, $i));
                $iso = date('N', $date);
                if ($data[sprintf('%dDay', $iso)]) {
                    $line_data = array(
                        'Date' => date('Y-m-d', $date)
                    );
                    foreach ($line_fields as $field) {
                        $field_name = sprintf('%d%s', $iso, $field);
                        if (array_key_exists($field_name, $data)) {
                            $line_data[$field] = $data[$field_name];
                        }
                    }
                    $line = null;
                    try {
                        $line = $lines->filter('Date', date('Y-m-d', $date))->first();
                    }
                    catch (LogicException $ex) {
                        
                    }
                    if (is_null($line)) {
                        $line = TimesheetLine::create();
                    }
                    $line->update($line_data);
                    $lines->add($line);
                }
                else {
                    try {
                        $existing = $lines->filter('Date', date('Y-m-d', $date))->first();
                        if (!is_null($existing)) {
                            $existing->delete();
                        }
                    }
                    catch (LogicException $ex) {
                        
                    }
                }
                $i--;
            }
        }
        else {
            parent::saveInto($obj, $fields);
        }
    }
}