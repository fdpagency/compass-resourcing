<?php


class Compass_MemberLoginForm extends MemberLoginForm {
	
	public function forgotPassword($data) {
		// Ensure password is given
		if(empty($data['Email'])) {
			$this->sessionMessage(
				_t('Member.ENTEREMAIL', 'Please enter an email address to get a password reset link.'),
				'bad'
			);

			$this->controller->redirect('Security/lostpassword');
			return;
		}

		// Find existing member
		$member = Member::get()->filter("Email", $data['Email'])->first();

		// Allow vetoing forgot password requests
		$results = $this->extend('forgotPassword', $member);
		if($results && is_array($results) && in_array(false, $results, true)) {
			return $this->controller->redirect('Security/lostpassword');
		}

		if($member) {
			$token = $member->generateAutologinTokenAndStoreHash();

			$e = ForgotPasswordEmail::create($member);
			$e->send();

			$this->controller->redirect('Security/passwordsent/' . urlencode($data['Email']));
		} elseif($data['Email']) {
			// Avoid information disclosure by displaying the same status,
			// regardless wether the email address actually exists
			$this->controller->redirect('Security/passwordsent/' . rawurlencode($data['Email']));
		} else {
			$this->sessionMessage(
				_t('Member.ENTEREMAIL', 'Please enter an email address to get a password reset link.'),
				'bad'
			);

			$this->controller->redirect('Security/lostpassword');
		}
	}
}