<?php


class TimesheetStatementsForm extends Form {
    
    public static function create() {
        $args = func_get_args();
        $controller = $args[0];
        $form = new TimesheetStatementsForm(
            $controller,
            'TimesheetStatementsForm',
            new FieldList(
                CheckboxField::create('Approved', 'I have read and fully understand the statements above and agree to abide by them all'),
				HiddenField::create('Type', 'Type', $controller->Type()->Name)
            ),
            new FieldList(
                new FormAction('SubmitStatements', 'Proceed')
            ),
            new FormValidator(array(
                'Approved' => array('required' => 'You must acknowledge you understand all the above statements before you may proceed')
            ))
        );
        $form->setTemplate(get_class($form));
        Session::clear(sprintf('FormInfo.%s', $form->FormName()));
        return $form;
    }
}