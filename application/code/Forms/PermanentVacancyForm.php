<?php


class PermanentVacancyForm extends Form {
    
    public static function create() {
        $args = func_get_args();
        $controller = $args[0];
        $form = new PermanentVacancyForm(
            $controller,
            'PermanentVacancyForm',
            new FieldList(
                TextField::create('Name', 'Name'),
                EmailField::create('Email', 'Email Address'),
                TextField::create('Phone', 'Phone'),
                TextareaField::create('Comments', 'Comments')
            ),
            new FieldList(
                new FormAction('SubmitPermanentVacancy', 'Submit')
            ),
            new FormValidator(array(
                'Name' => array(
					'required' => 'Please enter your name'
				),
                'Email' => array(
					'required' => 'Please enter your email address',
					'email_address' => 'Please enter a valid email address'
				),
                'Phone' => array(
					'required' => 'Please enter your telephone number'
				),
                'Comments' => array(
					'required' => 'Please provide some details'
				)
            ))
        );
        $form->setTemplate(get_class($form));
        Session::clear(sprintf('FormInfo.%s', $form->FormName()));
        return $form;
    }
}