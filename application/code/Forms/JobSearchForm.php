<?php


class JobSearchForm extends Form {
	
	public static function create() {
		$args = func_get_args();
        $controller = $args[0];
		$full = (count($args) > 1) ? $args[1] : true;
		$fields = array(
			TextField::create('Keywords', 'Keywords'),
            TextField::create('Location', 'Location')
		);
		$actions = array(
			FormAction::create('Search', 'Search')
		);
		if ($full) {
			$fields = array_merge(
				$fields, array(
					NumericField::create('MinPay', 'Minimum Salary'),
					NumericField::create('MaxPay', 'Maximum Salary'),
					DropdownField::create('PaySchedule', 'Pay Schedule', singleton('Job')->dbObject('PaySchedule')->enumValues()),
					CheckboxSetField::create('JobType', 'Job Type', singleton('Job')->dbObject('JobType')->enumValues()),
					CheckboxSetField::create('EmploymentType', 'Employment Type', singleton('Job')->dbObject('EmploymentType')->enumValues())
				)
			);
			$actions = array_merge(
				$actions, array(
					FormAction::create('Reset', 'Reset')->addExtraClass('reset')
				)
			);
		}
        $form = new JobSearchForm(
            $controller,
            'JobSearchForm',
            new FieldList($fields),
            new FieldList($actions),
            new FormValidator(array())
        );
		$form->setHTMLID('refineJobs');
        $form->setTemplate(get_class($form));
        $form->disableSecurityToken();
        $form->setFormMethod('GET');
        $form->setFormAction($controller->JobsPage()->Link());
		if ($data = $controller->getSearchData()) {
			$form->loadDataFrom($data, Form::MERGE_IGNORE_FALSEISH);
		}
        Session::clear(sprintf('FormInfo.%s', $form->FormName()));
        return $form;
	}
}