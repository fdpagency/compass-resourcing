<?php


class CVForm extends Form {
    
    public static function create() {
        $args = func_get_args();
        $controller = $args[0];
        $form = new CVForm(
            $controller,
            'cvform',
            new FieldList(
                TextField::create('Name', 'Name'),
                EmailField::create('Email', 'Email Address'),
                TextField::create('Phone', 'Phone'),
                TextField::create('Mobile', 'Mobile'),
                TextField::create('Postcode', 'Postcode'),
                FileField::create('File', 'CV upload')
            ),
            new FieldList(
                new FormAction('SubmitCV', 'Submit')
            ),
            new FormValidator(array(
                'Name' => array('required'),
                'Email' => array('required', 'email_address'),
                'Phone' => array('required'),
                'Mobile' => array('required'),
                'Postcode' => array('required'),
                'File' => array('required')
            ))
        );
        $form->setTemplate(get_class($form));
        Session::clear(sprintf('FormInfo.%s', $form->FormName()));
        return $form;
    }
}