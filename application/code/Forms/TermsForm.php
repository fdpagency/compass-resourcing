<?php


class TermsForm extends Form {
    
    public static function create() {
        $args = func_get_args();
        $controller = $args[0];
        $form = new TermsForm(
            $controller,
            'TermsForm',
            new FieldList(
				CheckboxField::create('Approved', 'I agree to the Compass Resourcing Ltd Terms of Business'),
                TextField::create('FirstName', 'First Name'),
				TextField::create('MiddleName', 'Middle Name'),
				TextField::create('Surname', 'Surname'),
                DateField::create('DOB', 'Date of Birth'),
				TextField::create('JobTitle', 'Job Title'),
				TextField::create('Company', 'Company'),
				DateField::create('Today', 'Today\'s Date'),
				EmailField::create('Email', 'Email Address')
            ),
            new FieldList(
                new FormAction('ApproveTerms', 'Submit')
            ),
            new FormValidator(array(
				'Approved' => array(
					'required' => 'Please tick the box to confirm you accept our Terms of Business'
				),
                'FirstName' => array(
					'required' => 'Please enter your first name'
				),
				'Surname' => array(
					'required' => 'Please enter your surname'
				),
				'DOB' => array(
					'required' => 'Please enter your date of birth (dd/mm/yyyy)'
				),
				'JobTitle' => array(
					'required' => 'Please enter your job title'
				),
				'Company' => array(
					'required' => 'Please enter the name of the company you are working for'
				),
				'Today' => array(
					'required' => 'Please enter today\'s date (dd/mm/yyyy)'
				),
                'Email' => array(
					'required' => 'Please enter your email address',
					'email_address' => 'Please enter a valid email address'
				)
            ))
        );
        $form->setTemplate(get_class($form));
        Session::clear(sprintf('FormInfo.%s', $form->FormName()));
        return $form;
    }
}