<?php


class JobApplicationForm extends Form {
    
    public static function create() {
        $args = func_get_args();
        $controller = $args[0];
		$job = $args[1];
        $form = new JobApplicationForm(
            $controller,
            'JobApplicationForm',
            new FieldList(
                TextField::create('Name', 'Name'),
                EmailField::create('Email', 'Email Address'),
                TextField::create('Phone', 'Telephone Number'),
                TextareaField::create('CoveringLetter', 'Covering Letter'),
                FileField::create('File', 'CV'),
				HiddenField::create('JobID', 'JobID', $job->ID)
            ),
            new FieldList(
                FormAction::create('SubmitApplication', 'Apply')->addExtraClass('back')
            ),
            new FormValidator(array(
                'Name' => array(
					'required' => 'Please enter your name'
				),
                'Email' => array(
					'required' => 'Please enter your email address',
					'email_address' => 'Please enter a valid email address'
				),
                'Phone' => array(
					'required' => 'Please enter your telephone number',
					'telephone' => 'Please enter a valid telephone number'
				),
                'File' => array(
					'required' => 'Please upload your CV'
				)
            ))
        );
        Session::clear(sprintf('FormInfo.%s', $form->FormName()));
        return $form;
    }
}