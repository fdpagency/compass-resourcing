<?php


class RegistrationForm extends Form {
    
    public static function create() {
        $args = func_get_args();
        $controller = $args[0];
        $registration = $args[1];
        $form = new RegistrationForm(
            $controller,
            'registrationform',
            $registration->getSectionFields($controller),
            $registration->getSectionActions(),
            $registration->getSectionValidator()
        );
        $form->setTemplate(get_class($form));
        $form->loadDataFrom($registration->getSectionData(), Form::MERGE_IGNORE_FALSEISH);
        $form->loadDataFrom($registration, Form::MERGE_IGNORE_FALSEISH);
        Session::clear(sprintf('FormInfo.%s', $form->FormName()));
        return $form;
    }
    
    public function validate() {
        if (Form::current_action() == 'PreviousSection') {
        	return true;
    	}
    	else {
        	return parent::validate();
    	}
	}
}