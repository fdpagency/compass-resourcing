<?php


class TimesheetApprovalForm extends Form {
    
    public static function create() {
        $args = func_get_args();
        $controller = $args[0];
        $timesheets = $args[1]->toArray();
        
        $actions = array();
        foreach ($timesheets as $timesheet) {
            $action = FormAction::create(sprintf('Approve?ID=%d', $timesheet->ID), 'Approve')->addExtraClass('submitButton');
            $timesheet->Action = $action;
            $actions[] = $action;
        }
        
        $form = new TimesheetApprovalForm(
            $controller,
            'TimesheetApprovalForm',
            FieldList::create(),
            FieldList::create($actions),
            new FormValidator(array())
        );
        $form->setTimesheets($timesheets);
        return $form;
    }
    
    private $_timesheets;
    
    public function Timesheets() {
        return $this->_timesheets;
    }
    
    public function setTimesheets($timesheets) {
        $this->_timesheets = ArrayList::create($timesheets);
    }
}