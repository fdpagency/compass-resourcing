<?php


class TimesheetApprovalField extends FormField {
    
    public static function create() {
        $args = func_get_args();
        return new TimesheetApprovalField($args[0]);
    }
    
    public function __construct($name) {
        parent::__construct($name);
    }
    
    private $_timesheets;
    
    public function Timesheets() {
        if (is_null($this->_timesheets)) {
            $this->_timesheets = Timesheet::get();
        }
        return $this->_timesheets;
    }
}