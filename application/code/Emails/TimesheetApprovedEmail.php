<?php


class TimesheetApprovedEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $timesheet = $args[0];
        $recipients = $args[1];
        $email = new TimesheetApprovedEmail(
			$recipients,
            sprintf('A %s timesheet for %s has been approved by %s', $timesheet->Type, $timesheet->Name, $timesheet->SignOff1()->Name)
        );
        $email->populateTemplate($timesheet);
		return $email;
    }
}