<?php

class CompassEmail extends ThemedEmail {
    
    public function __construct($recipients, $subject) {
        parent::__construct('info@compassr.co.uk', $recipients, $subject);
		$this->customHeaders['X-MC-Subaccount'] = Config::inst()->get('SmtpMailer', 'account');
    }
}