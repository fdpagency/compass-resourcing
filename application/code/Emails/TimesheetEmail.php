<?php


class TimesheetEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $timesheet = $args[0];
        $recipients = $args[1];
        $email = new TimesheetEmail(
			$recipients,
            sprintf('A %s timesheet has been submitted by %s', $timesheet->Type, $timesheet->Name)
        );
		$email->attachFileFromString(
			$timesheet->output(),
			sprintf(
				'%s-%s.xlsx',
				preg_replace('/[^a-zA-Z0-9]*/', '', $timesheet->Name),
				date('dmY', strtotime($timesheet->EndDate))
			),
			'application/vnd.ms-excel'
		);
        $email->populateTemplate($timesheet);
		return $email;
    }
}