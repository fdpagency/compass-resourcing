<?php


class RegistrationPackEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $pack = $args[0];
        $recipients = $args[1];
        $email = new RegistrationPackEmail(
			$recipients,
            sprintf('A %s registration pack has been submitted by %s', $pack->Type, $pack->FullName())
        );
        $pdf = $pack->pdf(false);
        $email->attachFileFromString($pdf->output(), $pdf->Filename, 'application/pdf');
        foreach ($pack->Files() as $file) {
            $email->attachFile($file->AbsolutePath(), $file->Name);
        }
        $email->populateTemplate($pack);
		return $email;
    }
}