<?php


class TimesheetSignOffEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $timesheets = $args[0];
        $member = $args[1];
        $email = new TimesheetSignOffEmail(
			$member->Email,
            sprintf('%d timesheet%s have been sent to you for sign off', $timesheets->count(), ($timesheets->count() != 1) ? 's' : '')
        );
        $email->populateTemplate(ArrayData::create(array(
            'Member' => $member,
            'Timesheets' => $timesheets,
            'SignOffPage' => TimesheetSignOffPage::get()->first()
        )));
		return $email;
    }
}