<?php


class CVEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $cv = $args[0];
        $recipients = $args[1];
        $email = new CVEmail(
			$recipients,
            sprintf('A CV has been received from %s', $cv->Name)
        );
        $email->attachFile($cv->File->tmp_name, $cv->File->name);
        $email->populateTemplate($cv);
		return $email;
    }
}