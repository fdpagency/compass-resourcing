<?php


class MedicalQuestionnaireThankYouEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $questionnaire = $args[0];
        if (!empty($questionnaire->Email)) {
            $email = new MedicalQuestionnaireThankYouEmail(
                $questionnaire->Email,
                'Thank you for submitting your medical questionnaire to Compass Resourcing'
            );
            $pdf = $questionnaire->pdf(false);
            $email->attachFileFromString($pdf->output(), $pdf->Filename, 'application/pdf');
            $email->populateTemplate($questionnaire);
            return $email;
        }
        return null;
    }
}