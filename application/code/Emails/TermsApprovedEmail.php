<?php


class TermsApprovedEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $terms = $args[0];
		$approval = $args[1];
        $recipients = $args[2];
        $email = new TermsApprovedEmail(
			$recipients,
            sprintf('%s %s has approved Terms of Business V%d', $approval->FirstName, $approval->Surname, $terms->Version)
        );
        $email->attachFileFromString(file_get_contents($terms->File()->getFullPath()), $terms->File()->Name, 'application/pdf');
        $email->populateTemplate(ArrayData::create(array(
			'Terms' => $terms,
			'Approval' => $approval
		)));
		return $email;
    }
}