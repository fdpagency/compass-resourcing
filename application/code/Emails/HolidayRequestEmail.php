<?php


class HolidayRequestEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $request = $args[0];
        $recipients = $args[1];
        $email = new HolidayRequestEmail(
			$recipients,
            sprintf('%s has submitted a holiday request', $request->FullName())
        );
        $pdf = $request->pdf(false);
        $email->attachFileFromString($pdf->output(), $pdf->Filename, 'application/pdf');
        $email->populateTemplate($request);
		return $email;
    }
}