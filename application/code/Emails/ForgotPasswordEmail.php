<?php


class ForgotPasswordEmail extends CompassEmail {
    
	public static function create() {
        $args = func_get_args();
        $member = $args[0];
        $email = new ForgotPasswordEmail(
			$member->Email,
            'Reset your Compass Resourcing Ltd password'
        );
        $email->populateTemplate($member);
		return $email;
    }
}