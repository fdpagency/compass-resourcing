<?php


class JobApplicationEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $application = $args[0];
        $email = new JobApplicationEmail(
			$application->Job->Manager()->Email,
            sprintf(
				'A job application has been received from %s regarding %s (%s)',
				$application->Name,
				$application->Job->Title,
				$application->Job->Reference
			)
        );
        $email->attachFile($application->File->tmp_name, $application->File->name);
        $email->populateTemplate($application);
		return $email;
    }
}