<?php


class TimesheetThankYouEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $timesheet = $args[0];
        $email = new TimesheetThankYouEmail(
            $timesheet->Email,
            sprintf('Thank you for submitting your %s timesheet', $timesheet->Type)
        );
		$email->attachFileFromString(
			$timesheet->output(),
			sprintf(
				'%s-%s.xlsx',
				preg_replace('/[^a-zA-Z0-9]*/', '', $timesheet->Name),
				date('dmY', strtotime($timesheet->EndDate))
			),
			'application/vnd.ms-excel'
		);
        $email->populateTemplate($timesheet);
        return $email;
    }
}