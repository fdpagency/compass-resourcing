<?php


class MedicalQuestionnaireEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $questionnaire = $args[0];
        $recipients = $args[1];
        $email = new MedicalQuestionnaireEmail(
			$recipients,
            sprintf('%s has submitted a medical questionnaire', $questionnaire->FullName())
        );
        $pdf = $questionnaire->pdf(false);
        $email->attachFileFromString($pdf->output(), $pdf->Filename, 'application/pdf');
        $email->populateTemplate($questionnaire);
		return $email;
    }
}