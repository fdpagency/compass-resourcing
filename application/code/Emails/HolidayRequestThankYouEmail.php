<?php


class HolidayRequestThankYouEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $request = $args[0];
        if (!empty($request->Email)) {
            $email = new HolidayRequestThankYouEmail(
                $request->Email,
                'Thank you for submitting your holiday request to Compass Resourcing'
            );
            $pdf = $request->pdf(false);
            $email->attachFileFromString($pdf->output(), $pdf->Filename, 'application/pdf');
            $email->populateTemplate($request);
            return $email;
        }
        return null;
    }
}