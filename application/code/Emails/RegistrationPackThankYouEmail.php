<?php


class RegistrationPackThankYouEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $pack = $args[0];
        if (!empty($pack->Email)) {
            $email = new RegistrationPackThankYouEmail(
                $pack->Email,
                sprintf('Thank you for submitting your %s registration pack to Compass Resourcing', $pack->Type)
            );
            $pdf = $pack->pdf(false);
            $email->attachFileFromString($pdf->output(), $pdf->Filename, 'application/pdf');
            foreach ($pack->Files() as $file) {
                $email->attachFile($file->AbsolutePath(), $file->Name);
            }
            $email->populateTemplate($pack);
            return $email;
        }
        return null;
    }
}