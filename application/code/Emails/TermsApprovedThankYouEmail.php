<?php


class TermsApprovedThankYouEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $terms = $args[0];
		$approval = $args[1];
        $email = new TermsApprovedThankYouEmail(
			$approval->Email,
            'Thank you for approving Compass Resourcing Ltd\'s Terms of Business'
        );
        $email->attachFileFromString(file_get_contents($terms->File()->getFullPath()), $terms->File()->Name, 'application/pdf');
        $email->populateTemplate(ArrayData::create(array(
			'Terms' => $terms,
			'Approval' => $approval
		)));
		return $email;
    }
}