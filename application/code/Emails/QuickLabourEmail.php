<?php


class QuickLabourEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $submission = $args[0];
        $recipients = $args[1];
        $email = new QuickLabourEmail(
			$recipients,
            sprintf('A quick labour order has been received from %s', $submission->Name)
        );
        $email->populateTemplate($submission);
		return $email;
    }
}