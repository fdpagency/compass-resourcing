<?php


class PermanentVacancyEmail extends CompassEmail {
    
    public static function create() {
        $args = func_get_args();
        $submission = $args[0];
        $recipients = $args[1];
        $email = new PermanentVacancyEmail(
			$recipients,
            sprintf('A permanent vacancy has been posted by %s', $submission->Name)
        );
        $email->populateTemplate($submission);
		return $email;
    }
}