<?php


class RegistrationManager extends ModelAdmin {
    
    private static $menu_title = 'Registrations';
    private static $url_segment = 'registrations';
    private static $menu_icon = 'application/images/RegistrationManager.png';
    private static $managed_models = array(
        'RegistrationPack' => array(
            'title' => 'Registration Packs'
        ),
        'MedicalQuestionnaire' => array(
            'title' => 'Medical Questionnaires'
        ),
        'HolidayRequest' => array(
            'title' => 'Holiday Requests'
        ),
        'Nationality' => array(
            'title' => 'Nationalities'
        ),
        'CSCSCard' => array(
            'title' => 'CSCS Cards'
        ),
        'ConstructionQualification' => array(
            'title' => 'Qualifications'
        )
    );
    private static $model_importers = array(
        'Nationality' => 'CSVBulkloader',
        'CSCSCard' => 'CSVBulkloader',
        'ConstructionQualification' => 'CSVBulkloader'
    );
    
    public function getEditForm($id=null, $fields=null) {
        $form = parent::getEditForm($id, $fields);
        foreach (array('RegistrationPack', 'MedicalQuestionnaire', 'HolidayRequest') as $registration) {
            if ($field = $form->Fields()->dataFieldByName($registration)) {
                if ($field instanceof GridField) {
                    $config = GridFieldConfig::create();
                    $config->addComponents(
                        new GridFieldSortableHeader(),
                        new GridFieldDataColumns(),
                        new GridFieldPaginator(50),
                        new GridFieldRegistrationDownloader(),
                        new GridFieldDeleteAction()
                    );
                    $field->setConfig($config);
                }
            }
        }
        return $form;
    }
    
    public function getList() {
        $list = parent::getList();
        if (in_array($this->modelClass, array('RegistrationPack', 'MedicalQuestionnaire', 'HolidayRequest'))) {
            $list = $list->where('Completed IS NOT NULL')->sort('Completed DESC, Created DESC');
        }
        return $list;
    }
}

class GridFieldRegistrationDownloader implements GridField_ColumnProvider, GridField_ActionProvider {
    
    public function augmentColumns($grid, &$columns) {
        if (!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }
    
    public function getColumnAttributes($grid, $record, $column) {
        return array('class' => 'col-buttons');
    }
    
    public function getColumnMetadata($grid, $column) {
        if ($column == 'Actions') {
            return array('title' => '');
        }
    }
    
    public function getColumnsHandled($grid) {
        return array('Actions');
    }
    
    public function getColumnContent($grid, $record, $column) {
        $field = GridField_FormAction::create(
            $grid,
            'DownloadRegistration' . $record->ID,
            'Download PDF',
            'DownloadRegistration',
            array('ID' => $record->ID)
        )->setAttribute('data-icon', 'disk')->addExtraClass('no-ajax');
        return $field->Field();
    }
    
    public function getActions($grid) {
        return array('DownloadRegistration');
    }
    
    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == 'downloadregistration') {
            $pack = $grid->getList()->filter('ID', $args['ID'])->first();
            if (!is_null($pack)) {
                $pack->pdf();
            }
        }
    }
}