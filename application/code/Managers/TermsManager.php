<?php


class TermsManager extends ModelAdmin {
    
    private static $menu_title = 'Terms';
    private static $url_segment = 'terms';
    private static $menu_icon = 'application/images/TermsManager.png';
    private static $managed_models = array(
        'Terms' => array(
            'title' => 'Terms of Business'
        )
    );
	
	public function getEditForm($id=null, $fields=null) {
        $form = parent::getEditForm($id, $fields);
		if ($field = $form->Fields()->dataFieldByName('Terms')) {
			if ($field instanceof GridField) {
				$config = $field->getConfig();
				$config->addComponents(new GridFieldTermsDownloader());
				$field->setConfig($config);
			}
		}
        return $form;
    }
}

class GridFieldTermsDownloader implements GridField_ColumnProvider, GridField_ActionProvider {
    
    public function augmentColumns($grid, &$columns) {
        if (!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }
    
    public function getColumnAttributes($grid, $record, $column) {
        return array('class' => 'col-buttons');
    }
    
    public function getColumnMetadata($grid, $column) {
        if ($column == 'Actions') {
            return array('title' => '');
        }
    }
    
    public function getColumnsHandled($grid) {
        return array('Actions');
    }
    
    public function getColumnContent($grid, $record, $column) {
        $field = GridField_FormAction::create(
            $grid,
            'DownloadTerms' . $record->ID,
            'Download PDF',
            'DownloadTerms',
            array('ID' => $record->ID)
        )->setAttribute('data-icon', 'disk')->addExtraClass('no-ajax');
        return $field->Field();
    }
    
    public function getActions($grid) {
        return array('DownloadTerms');
    }
    
    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == 'downloadterms') {
            $terms = $grid->getList()->filter('ID', $args['ID'])->first();
            if (!is_null($terms)) {
                $terms->pdf();
            }
        }
    }
}