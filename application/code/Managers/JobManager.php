<?php


class JobManager extends ExportableModelAdmin {
    
    private static $menu_title = 'Jobs';
    private static $url_segment = 'jobs';
    private static $menu_icon = 'application/images/JobManager.png';
    private static $managed_models = array(
        'Job' => array(
            'title' => 'Jobs'
        ),
        'Industry' => array(
            'title' => 'Industries'
        )
    );
    private static $model_importers = array(
		'Job' => 'JobBulkLoader',
		'Industry' => 'CSVBulkloader'
    );
}