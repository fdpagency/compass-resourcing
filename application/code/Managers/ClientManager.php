<?php


class ClientManager extends ModelAdmin {
    
    private static $menu_title = 'Clients';
    private static $url_segment = 'clients';
    private static $menu_icon = 'application/images/ClientManager.png';
    private static $managed_models = array(
        'Client' => array(
            'title' => 'Clients'
        )
    );
}
