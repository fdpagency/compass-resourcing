<?php


class HomePageJob extends DataObject {
    
    private static $db = array(
        'Title' => 'Varchar(200)',
        'Link' => 'Varchar(200)',
        'Location' => 'Varchar(200)',
        'Salary' => 'Varchar(200)',
        'Type' => "Enum('Temporary,Permanent')",
        'Summary' => 'Text',
        'Index' => 'Int'
    );
    private static $has_one = array(
        'Page' => 'HomePage'
    );
    private static $default_sort = 'Index ASC';
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName('Index');
        $fields->removeByName('PageID');
        return $fields;
    }
}
