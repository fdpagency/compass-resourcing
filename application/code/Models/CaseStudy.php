<?php

class CaseStudy extends DataObject {

    public static $db = array(
        'SortOrder'=>'Int',
        'Info' => 'Text'
    );

    public static $has_one = array(
        'Image' => 'Image',
        'CaseStudiesPage' => 'CaseStudiesPage'
    );

    public static $summary_fields = array(
        'Thumbnail' => 'Thumbnail',
        'Info' => 'Info'
    );

    public static $default_sort='SortOrder';

    public function getCMSFields() {

        $textField = new TextareaField('Info', 'Info');

        $sizeMB = 2;
        $size = $sizeMB * 1024 * 1024;

        $imageField = new UploadField('Image', 'Image');
        $imageField->getValidator()->setAllowedMaxFileSize($size);
        $imageField->allowedExtensions = array('jpg','png');

        $classnameField = new FieldList(
            $imageField,
            $textField
        );

        return $classnameField;
    }

    public function getThumbnail() {
        return $this->Image()->CMSThumbnail();
    }

}