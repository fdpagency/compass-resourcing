<?php


class TimesheetLine extends DataObject {
    
    private static $db = array(
        'Date' => 'Date',
        'JobNumber' => 'Varchar(200)',
        'JobTitle' => 'Varchar(200)',
        'Start' => 'Time',
        'End' => 'Time',
        'Breaks' => 'Float',
        'HoursDays' => 'Float',
        'HoursNights' => 'Float'
    );
    private static $has_one = array(
        'Timesheet' => 'Timesheet'
    );
    private static $default_sort = 'Date ASC';
    
    private static $summary_fields = array(
        'Date.Nice' => 'Date',
		'JobNumber' => 'Job Number',
        'JobTitle' => 'Position',
        'Start' => 'Start Time',
        'End' => 'End Time',
        'Breaks' => 'Breaks',
        'HoursDays' => 'Total Hours Days',
        'HoursNights' => 'Total Hours Nights',
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName('TimesheetID');
        return $fields;
    }
}