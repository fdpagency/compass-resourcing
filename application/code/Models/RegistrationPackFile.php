<?php


class RegistrationPackFile extends DataObject {
    
    private static $db = array(
        'Name' => 'Varchar(200)',
        'Extension' => 'Varchar(10)',
        'Description' => 'Varchar(200)'
    );
    private static $has_one = array(
        'Pack' => 'RegistrationPack'
    );
    private static $default_sort = 'Created ASC';
    
    public function onBeforeWrite() {
        parent::onBeforeWrite();
        if (preg_match('/\.([a-zA-Z0-9]*)$/', $this->Name, $matches)) {
            $this->Extension = $matches[1];
        }
    }
    
    public function canCreate($member = null) {
		return false;
	}
    
    public function canEdit($member = null) {
		return false;
	}
    
    public function canDelete($member = null) {
		return false;
	}
    
    public function onBeforeDelete() {
        parent::onBeforeDelete();
        if (file_exists($this->AbsolutePath())) {
            unlink($this->AbsolutePath());
        }
    }
	
	public function AbsolutePath() {
    	return sprintf('%s/tmp/%d.%s', $_SERVER['DOCUMENT_ROOT'], $this->ID, $this->Extension);
	}
	
	public function save($contents) {
    	file_put_contents($this->AbsolutePath(), $contents);
	}
	
	public function read() {
    	return file_get_contents($this->AbsolutePath());
	}
}