<?php

class Office extends DataObject {

    public static $db = array(
        'SortOrder'=>'Int',
        'City' => 'Text',
        'Address' => 'Text',
        'PhoneNumber' => 'Text',
        'Fax' => 'Text',
        'Email' => 'Text'
    );

    public static $has_one = array(
        'ContactPage' => 'ContactPage'
    );

    public static $summary_fields = array(
        'City'=>'City'
    );

    public static $default_sort='SortOrder';

    public function getCMSFields() {

        $textFieldCity = new TextField('City', 'City');
        $textFieldAddress = new TextareaField('Address', 'Address');
        $textFieldPhone = new TextField('PhoneNumber', 'PhoneNumber');
        $textFieldFax = new TextField('Fax', 'Fax');
        $textFieldEmail = new TextField('Email', 'Email');

        return new FieldList(
            $textFieldCity,
            $textFieldAddress,
            $textFieldPhone,
            $textFieldFax,
            $textFieldEmail
        );
    }

}