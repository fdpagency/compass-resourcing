<?php


class Nationality extends DataObject {
    
    public static function form_map() {
        return Nationality::get()->map('ID', 'Title');
    }
    
    private static $db = array(
        'Title' => 'Varchar(200)',
        'Default' => 'Boolean'
    );
    private static $default_sort = 'Title ASC';
    private static $summary_fields = array(
        'Title' => 'Title',
        'Default.Nice' => 'Default?'
    );
    
    public function onBeforeWrite() {
        parent::onBeforeWrite();
        if ($this->Default) {
            foreach (Nationality::get()->filter('Default', true) as $nationality) {
                $nationality->Default = false;
                $nationality->write();
            }
        }
    }
}