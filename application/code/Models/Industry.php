<?php


class Industry extends DataObject {
	
	private static $db = array(
		'Name' => 'Varchar(200)'
	);
	private static $slugged_field = array('Name');
	private static $default_sort = 'Name ASC';
}