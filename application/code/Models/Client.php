<?php


class Client extends DataObject {
    
    private static $db = array(
        'Name' => 'Varchar(200)'
    );
    private static $has_many = array(
        'Members' => 'Member'
    );
	private static $default_sort = 'Name ASC';
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName('Members');
        if ($this->ID > 0) {
			$member_columns = new GridFieldDataColumns();
			$member_columns->setDisplayFields(array(
				'FirstName' => 'First Name',
				'Surname' => 'Surname',
				'Email' => 'Email / Username',
				'TermsStatus' => 'Terms Agreed',
				'TermsDate.Nice' => 'Date Terms Agreed'
			));
            $fields->addFieldToTab(
                'Root.Main',
                GridField::create('Members', 'Members', $this->Members(), GridFieldConfig::create()->addComponents(
                    new GridFieldToolbarHeader(),
                    new GridFieldAddNewButton('toolbar-header-right'),
                    new GridFieldSortableHeader(),
                    $member_columns,
                    new GridFieldEditButton(),
                    new GridFieldDeleteAction(),
					new GridFieldPasswordReset(),
                    new GridFieldDetailForm()
                ))
            );
        }
        return $fields;
    }
}

class GridFieldPasswordReset implements GridField_ColumnProvider, GridField_ActionProvider {
    
    public function augmentColumns($grid, &$columns) {
        if (!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }
    
    public function getColumnAttributes($grid, $record, $column) {
        return array('class' => 'col-buttons');
    }
    
    public function getColumnMetadata($grid, $column) {
        if ($column == 'Actions') {
            return array('title' => '');
        }
    }
    
    public function getColumnsHandled($grid) {
        return array('Actions');
    }
    
    public function getColumnContent($grid, $record, $column) {
        $field = GridField_FormAction::create(
            $grid,
            'ResetPassword' . $record->ID,
            'Send Password Reset',
            'ResetPassword',
            array('ID' => $record->ID)
        )->setAttribute('data-icon', 'drive-upload');
        return $field->Field();
    }
    
    public function getActions($grid) {
        return array('ResetPassword');
    }
    
    public function handleAction(GridField $grid, $action, $args, $data) {
        if ($action == 'resetpassword') {
            $member = $grid->getList()->filter('ID', $args['ID'])->first();
            if (!is_null($member)) {
				$email = ForgotPasswordEmail::create($member);
				$email->send();
            }
        }
    }
}