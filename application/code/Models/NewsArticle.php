<?php


class NewsArticle extends DataObject {
    
    private static $db = array(
        'Title' => 'Varchar(250)',
        'Date' => 'Date',
        'Body' => 'HTMLText'
    );
    private static $has_one = array(
        'Page' => 'NewsPage',
        'Image' => 'Image'
    );
	private static $default_sort = 'Date DESC';
    private static $summary_fields = array(
		'Title' => 'Title',
        'Date.Nice' => 'Date'
	);
	private static $defaults = array(
    	'Published' => true
	);
    private static $slugged_field = array('Title');
    
    public function getCMSFields() {
    	$fields = parent::getCMSFields();
    	$fields->removeByName('PageID');
    	$fields->addFieldToTab(
        	'Root.Main',
        	UploadField::create('Image', 'Image')->setFolderName('News'),
        	'Body'
    	);
        return $fields;
	}
}