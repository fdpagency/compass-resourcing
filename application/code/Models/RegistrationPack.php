<?php


class RegistrationPack extends DataObject {
    
    private static $session_key = 'RegistrationPack';
    
    public static function retrieve() {
        return Compass_Registration::retrieve(get_called_class());
    }
    
    private static $db = array(
        'Section' => "Enum('Personal,Qualifications,Work History,Medical History,Payment Method,PAYE Procedure,PAYE Payment Details,PAYE Pension Scheme,PAYE Agreement,Umbrella Procedure,Limited Company Procedure,CIS Procedure,CIS Experience,CIS Previous Employment,CIS Declaration,CIS Payment Details,File Upload,Complete')",
        'Type' => "Enum(',PAYE,Umbrella,Limited Company,CIS','')",
        
        /* Personal Details */
        'Title' => "Enum('Mr,Mrs,Miss,Ms')",
        'Name' => 'Varchar(200)',
        'Address1' => 'Varchar(200)',
        'Address2' => 'Varchar(200)',
        'City' => 'Varchar(200)',
        'County' => 'Varchar(200)',
        'Postcode' => 'Varchar(20)',
        'DOB' => 'Date',
        'Phone' => 'Varchar(20)',
        'Mobile' => 'Varchar(20)',
        'Email' => 'Varchar(254)',
        'NationalInsurance' => 'Varchar(20)',
        'MaritalStatus' => "Enum('Single,Married,Civil Partnered,Divorced')",
        'DriversLicence' => 'Boolean',
        'WorkPermit' => 'Boolean',
        'JobCategory' => 'Varchar(200)',
        'DateAvailable' => 'Date',
        
        /* Qualifications */
        'WorkInConstruction' => 'Boolean',
        'CSCSExpiry' => 'Date',
        'CSCSCardNumber' => 'Varchar(200)',
        'AdditionalConstructionQualifications' => 'Text',
        'GeneralQualification1' => 'Varchar(200)',
        'GeneralQualification2' => 'Varchar(200)',
        'GeneralQualification3' => 'Varchar(200)',
        'GeneralQualification4' => 'Varchar(200)',
        'Skill1' => 'Varchar(200)',
        'Skill2' => 'Varchar(200)',
        'Skill3' => 'Varchar(200)',
        'Skill4' => 'Varchar(200)',
        'EducationalQualifications' => 'Text',
        
        /* Work History */
        'History1JobTitle' => 'Varchar(200)',
        'History1CompanyName' => 'Varchar(200)',
        'History1ContactName' => 'Varchar(200)',
        'History1ContactPhone' => 'Varchar(20)',
        'History1ContactPosition' => 'Varchar(200)',
        'History1ProjectName' => 'Varchar(200)',
        'History1Location' => 'Varchar(200)',
        'History1StartDate' => 'Date',
        'History1EndDate' => 'Date',
        'History1HourlyRate' => 'Varchar(100)',
        'History1AnnualSalary' => 'Varchar(100)',
        'History1Description' => 'Text',
        'History1ReasonLeft' => 'Text',
        'History2JobTitle' => 'Varchar(200)',
        'History2CompanyName' => 'Varchar(200)',
        'History2ContactName' => 'Varchar(200)',
        'History2ContactPhone' => 'Varchar(20)',
        'History2ContactPosition' => 'Varchar(200)',
        'History2ProjectName' => 'Varchar(200)',
        'History2Location' => 'Varchar(200)',
        'History2StartDate' => 'Date',
        'History2EndDate' => 'Date',
        'History2HourlyRate' => 'Varchar(100)',
        'History2AnnualSalary' => 'Varchar(100)',
        'History2Description' => 'Text',
        'History2ReasonLeft' => 'Text',
        'History3JobTitle' => 'Varchar(200)',
        'History3CompanyName' => 'Varchar(200)',
        'History3ContactName' => 'Varchar(200)',
        'History3ContactPhone' => 'Varchar(20)',
        'History3ContactPosition' => 'Varchar(200)',
        'History3ProjectName' => 'Varchar(200)',
        'History3Location' => 'Varchar(200)',
        'History3StartDate' => 'Date',
        'History3EndDate' => 'Date',
        'History3HourlyRate' => 'Varchar(100)',
        'History3AnnualSalary' => 'Varchar(100)',
        'History3Description' => 'Text',
        'History3ReasonLeft' => 'Text',
        
        /* Medical History */
        'Consciousness' => 'Boolean',
        'ConsciousnessDetails' => 'Text',
        'Bones' => 'Boolean',
        'BonesDetails' => 'Text',
        'Lungs' => 'Boolean',
        'LungsDetails' => 'Text',
        'Chest' => 'Boolean',
        'ChestDetails' => 'Text',
        'Vision' => 'Boolean',
        'VisionDetails' => 'Text',
        'Ears' => 'Boolean',
        'EarsDetails' => 'Text',
        'Metabolic' => 'Boolean',
        'MetabolicDetails' => 'Text',
        'Chronic' => 'Boolean',
        'ChronicDetails' => 'Text',
        'Treatment' => 'Boolean',
        'TreatmentDetails' => 'Text',
        'Disability' => 'Boolean',
        'DisabilityDetails' => 'Text',
        'Nights' => 'Boolean',
        'NightsDetails' => 'Text',
        'Medication' => 'Boolean',
        'MedicationDetails' => 'Text',
        'Convictions' => 'Boolean',
        'ConvictionsDetails' => 'Text',
        'DeclarationName' => 'Varchar(200)',
        'DeclarationDate' => 'Date',
        'EmergencyContactName' => 'Varchar(200)',
        'EmergencyContactPhone' => 'Varchar(20)',
        'EmergencyContactRelationship' => 'Varchar(200)',
        
        /* PAYE Procedure */
        'EmployeeStatement' => 'Text',
        'StudentLoan' => 'Boolean',
        'StarterChecklistName' => 'Varchar(200)',
        'StarterChecklistDate' => 'Varchar(200)',
        
        /* PAYE Payment Details */
        'AccountHolder' => 'Varchar(200)',
        'Bank' => 'Varchar(200)',
        'SortCode' => 'Varchar(200)',
        'AccountNumber' => 'Varchar(200)',
        'RollNumber' => 'Varchar(200)',
        'ThirdPartyName' => 'Varchar(200)',
        'ThirdPartyEmail' => 'Varchar(200)',
        
        /* PAYE Agreement */
        'PAYEAgreement' => 'Boolean',
        
        /* Umbrella Procedure */
        'UmbrellaPaymentSystem' => "Enum(',Pinnacle Sure Ltd,Walker Smith Contracting,Exchequer Solutions Ltd,Advance Consulting,Orbital','')",
        'UmbrellaPaymentSystemContact' => 'Boolean',
        
        /* CIS Procedure */
        'SelfEmployed' => 'Boolean',
        'HaveUTRNumber' => 'Boolean',
        'UTRNumber' => 'Varchar(20)',
        'UTRLength' => 'Varchar(200)',
        
        /* CIS Experience */
        'CISRoleA' => 'Varchar(200)',
        'CISRoleB' => 'Varchar(200)',
        'CISRoleResponsibilities' => 'Text',
        
        /* CIS Previous Employment */
        'LastJobPayment' => 'Varchar(200)',
        'LastJobPaymentCompany' => 'Varchar(200)',
        'Experience' => 'Varchar(200)',
        'ExperienceEvidence' => 'Boolean',
        'ExperienceEvidenceType' => 'Varchar(200)',
        
        /* CIS Declaration */
        'TradesmanExperience' => "Enum(',0 - 4 Years,4+ Years','')",
        'DeclarationDOB' => 'Date',
        
        /* CIS Payment Details */
        'CISPaymentSystem' => "Enum(',The Guild','')",
        'CISPaymentSystemContact' => 'Boolean',
        'Competent' => 'Boolean',
        'CISDeclaration' => 'Boolean',
    );
    private static $has_one = array(
        'Nationality' => 'Nationality',
        'CSCSCard' => 'CSCSCard',
        'ConstructionQualification1' => 'ConstructionQualification',
        'ConstructionQualification2' => 'ConstructionQualification',
        'ConstructionQualification3' => 'ConstructionQualification',
        'ConstructionQualification4' => 'ConstructionQualification'
    );
    private static $has_many = array(
        'Files' => 'RegistrationPackFile'
    );
    
    private static $defaults = array(
        'Section' => 'Personal'
    );
    private static $default_sort = 'Created DESC';
    
    private static $summary_fields = array(
        'FullName' => 'Name',
        'Type' => 'Type',
        'Completed.Nice' => 'Date'
    );
    private static $searchable_fields = array(
        'Name', 'Type', 'Completed'
    );
    
    public function canCreate($member = null) {
		return false;
	}
    
    public function canEdit($member = null) {
		return false;
	}
    
    public function FullName() {
        return sprintf('%s %s', $this->Title, $this->Name);
    }
    
    public function OutputTitle() {
        return sprintf('New %s Registration', $this->Type);
    }
}