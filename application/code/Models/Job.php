<?php


class Job extends DataObject {
	
	private static $db = array(
		'Title' => 'Varchar(200)',
		'Reference' => 'Varchar(200)',
		'Expires' => 'Date',
		'Description' => 'HTMLText',
		'Keywords' => 'Text',
		'Address1' => 'Varchar(200)',
		'Address2' => 'Varchar(200)',
		'Town' => 'Varchar(200)',
		'County' => 'Varchar(200)',
		'Postcode' => 'Varchar(20)',
		'Latitude' => 'Float',
		'Longitude' => 'Float',
		'JobType' => "Enum('Contract,Temporary,Permanent')",
		'EmploymentType' => "Enum('Full Time,Part Time')",
		'MinPay' => 'Float',
		'MaxPay' => 'Float',
		'PaySchedule' => "Enum('Per Hour,Per Day,Per Month,Per Year')",
		'Published' => 'Boolean'
	);
	private static $has_one = array(
		'Industry' => 'Industry',
		'Manager' => 'Member'
	);
	private static $slugged_field = array('Title');
	private static $default_sort = 'Created DESC';
	private static $defaults = array(
		'Published' => true
	);
	private static $summary_fields = array(
		'Title' => 'Title',
		'Reference' => 'Reference',
		'Expires.Nice' => 'Expires',
		'Manager.Name' => 'Manager',
		'Published.Nice' => 'Published?'
	);
	private static $export_fields = array(
		'Reference' => 'Reference',
		'Title' => 'Title',
		'Description' => 'Description',
		'Industry.Name' => 'Industry',
		'JobType' => 'Job Type',
		'EmploymentType' => 'Employment Type',
		'MinPay' => 'Salary From',
		'MaxPay' => 'Salary To',
		'PaySchedule' => 'Period',
		'Address1' => 'Address Line 1',
		'Address2' => 'Address Line 2',
		'Town' => 'Town/City',
		'County' => 'County',
		'Postcode' => 'Postcode',
		'Expires' => 'Expires',
		'Keywords' => 'Keywords',
		'Manager.Email' => 'Manager',
		'Published' => 'Published?'
	);
	
	public function onBeforeWrite() {
    	$address = array();
    	$address_updated = false;
    	foreach (array('Address1', 'Address2', 'Town', 'County', 'Postcode') as $field) {
        	if ($this->isChanged($field)) {
            	$address_updated = true;
        	}
        	$value = trim($this->$field);
        	if (!empty($value)) {
            	$address[] = $value;
        	}
    	}
    	if ($address_updated && count($address) > 1) {
        	$location = Geocoder::geocode(implode(',', $address));
        	if (!is_null($location)) {
            	$this->Latitude = $location->lat;
                $this->Longitude = $location->lng;
        	}
    	}
    	parent::onBeforeWrite();
	}
	
	public function getCMSFields() {
		return FieldList::create(
			CompositeField::create(
				TextField::create('Reference', 'Reference'),
				TextField::create('Title', 'Title'),
				HTMLEditorField::create('Description', 'Description')
			)->setTag('fieldset')->setLegend('Job Details')->addExtraClass('section'),
			CompositeField::create(
				OptionSetField::create(
					'IndustryID',
					'Industry',
					Industry::get()->map('ID', 'Name')
				),
				DropdownField::create('JobType', 'Job Type', $this->dbObject('JobType')->enumValues()),
				DropdownField::create('EmploymentType', 'Employment Type', $this->dbObject('EmploymentType')->enumValues())
			)->setTag('fieldset')->setLegend('Job Type')->addExtraClass('section'),
			CompositeField::create(
				TextField::create('MinPay', 'From (£)'),
				TextField::create('MaxPay', 'To (£)'),
				DropdownField::create('PaySchedule', 'Period', $this->dbObject('PaySchedule')->enumValues())
			)->setTag('fieldset')->setLegend('Salary Details')->addExtraClass('section'),
			CompositeField::create(
				TextField::create('Town', 'Town/City'),
				TextField::create('Postcode', 'Postcode')
			)->setTag('fieldset')->setLegend('Location')->addExtraClass('section'),
			CompositeField::create(
				DateField::create('Expires', 'Expires'),
				TextareaField::create('Keywords', 'Keywords'),
				DropdownField::create('ManagerID', 'Manager', Group::get()->filter('Code', 'administrators')->first()->Members()->map('ID', 'Name')),
				CheckboxSetField::create('Published', 'Published?', array(1 => 'Yes'))
			)->setTag('fieldset')->setLegend('Additional Information')->addExtraClass('section')
		);
	}
}