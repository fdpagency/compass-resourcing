<?php


class Terms extends DataObject {
	
	private static $_latest_version;
	
	public static function latest_version() {
		if (is_null(self::$_latest_version)) {
			self::$_latest_version = Terms::get()->max('Version');
		}
		return self::$_latest_version;
	}
	
	private static $db = array(
		'Version' => 'Int'
	);
	private static $has_one = array(
		'File' => 'File'
	);
	private static $belongs_many_many = array(
		'Members' => 'Member'
	);
	private static $default_sort = 'Version DESC';
	private static $summary_fields = array(
		'Created.Nice' => 'Date Issued',
		'Version' => 'Version'
	);
	
	protected function onBeforeWrite() {
		if ($this->Version < 1) {
			$this->Version = Terms::get()->max('Version') + 1;
		}
		parent::onBeforeWrite();
	}
	
	public function getCMSFields() {
		if ($this->ID < 1) {
			return FieldList::create(
				UploadField::create('File', 'File')->setFolderName('Terms'),
				LiteralField::create('Notice', '<p style="font-size: 1.2em; font-weight: bold; color: #cc0000">Once saved clients will have to approve these terms before to signing off further timesheets</p>')
			);
		}
		else {
			$fields = FieldList::create();
			if (!empty($this->Approved)) {
				$fields->push(
					CheckboxField::create('Approved', 'Approved?', $this->Approved)
				);
				$fields->push(
					TextField::create('Name', 'Name', sprintf('%s %s%s', $this->FirstName, empty($this->MiddleName) ? '' : sprintf('%s ', $this->MiddleName), $this->Surname))
				);
				$fields->push(
					DateField::create('DOB', 'Date of Birth', $this->DOB)
				);
				$fields->push(
					TextField::create('JobTitle', 'Job Title', $this->JobTitle)
				);
				$fields->push(
					TextField::create('Company', 'Company', $this->Company)
				);
				$fields->push(
					DateField::create('Today', 'Date Entered', $this->Today)
				);
				$fields->push(
					TextField::create('Email', 'Email', $this->Email)
				);
				$fields->push(
					DateTimeField::create('Date', 'Date Agreed', $this->Date)
				);
			}
			$fields->push(
				TextField::create('Version', 'Version')
			);
			$fields->push(
				HtmlEditorField_Readonly::create('DownloadFile', 'File', sprintf('<a href="%s" target="_blank">Download</a>', $this->File()->URL))
			);
			if (empty($this->Approved)) {
				$members = new ReadOnlyGridField('Members', 'Members', $this->Members());
				$members->getConfig()->getComponentByType('GridFieldDataColumns')->setDisplayFields(array(
					'FirstName' => 'First Name',
					'Surname' => 'Surname',
					'Email' => 'Email / Username',
					'Name' => 'Name Given',
					'AgreedDOB.Nice' => 'Date of Birth',
					'JobTitle' => 'Job Title',
					'Company' => 'Company',
					'AgreedToday.Nice' => 'Date Entered',
					'Email' => 'Email',
					'AgreedDate.Nice' => 'Date Agreed'
				));
				$fields->push($members);
			}
			return $fields;
		}
	}
	
	public function getBetterButtonsActions() {
        $fields = parent::getBetterButtonsActions();
		$fields->push(BetterButton_SaveAndClose::create());
        return $fields;
    }
	
	public function canCreate($member = null) {
		return true;
	}
	
	public function canEdit($member = null) {
		return $this->ID < 1;
	}
	
	public function canDelete($member = null) {
		return false;
	}
	
	private $_date_agreed;
	
	public function DateAgreed() {
		if (is_null($this->_date_agreed)) {
			if (!empty($this->Date)) {
				$this->_date_agreed = DBField::create_field('SS_Datetime', $this->Date);
			}
			else {
				$this->_date_agreed = '';
			}
		}
		return $this->_date_agreed;
	}
}