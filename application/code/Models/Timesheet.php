<?php


class Timesheet extends DataObject {
    
	public static function line_days($type) {
		$days = array();
		$range = array();
		switch ($type) {
			case 'Compass':
				$range = range(4, 10);
				break;
			case 'Cardinal':
				$range = range(2, 8);
				break;
		}
		foreach ($range as $day) {
			$seconds = 60 * 60 * 24 * $day;
			$days[date('l', $seconds)] = $seconds;
		}
		return $days;
	}
	
    private static $db = array(
        'Type' => "Enum('Compass,Cardinal')",
        'Name' => 'Varchar(200)',
        'Email' => 'Varchar(254)',
        'CompanyName' => 'Varchar(200)',
        'EndDate' => 'Date',
		'Location' => 'Text',
        'CandidateNotes' => 'Text',
        'ClientNotes' => 'Text',
		'SignedOff1' => 'SS_Datetime',
		'SignedOff2' => 'SS_Datetime',
        'Approved' => 'SS_Datetime',
		'Archived' => 'Boolean',
        'Status' => "Enum('Submitted,Awaiting Sign Off,Signed Off,Archived')"
    );
    private static $has_one = array(
        'SignOff1' => 'Member',
		'SignOff2' => 'Member'
    );
    private static $has_many = array(
        'Lines' => 'TimesheetLine'
    );
    private static $default_sort = 'Created DESC, EndDate DESC';
    
    private static $summary_fields = array(
        'EndDate.Nice' => 'Week Ending',
        'Name' => 'Candidate Name',
        'CompanyName' => 'Company',
		'Location' => 'Site',
        'TotalHours' => 'Total Hours',
        'SignOff1Summary' => 'Sign Off 1',
		'SignOff2Summary' => 'Sign Off 2'
    );
    
    private static $casting = array(
        'TotalHours' => 'Float',
		'TotalHoursDays' => 'Float',
		'TotalHoursNights' => 'Float',
		'TotalBreaks' => 'Float'
    );
    
    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName('Type');
        $fields->removeByName('Lines');
        $fields->removeByName('SignOff1ID');
		$fields->removeByName('SignOff2ID');
		$fields->removeByName('SignedOff1');
		$fields->removeByName('SignedOff2');
        $fields->removeByName('Approved');
        $fields->removeByName('Status');
        if ($this->ID > 0) {
            $fields->addFieldToTab(
                'Root.Main',
                GridField::create('Lines', 'Lines', $this->Lines(), GridFieldConfig::create()->addComponents(
                    new GridFieldToolbarHeader(),
                    new GridFieldAddNewButton('toolbar-header-right'),
                    new GridFieldSortableHeader(),
                    new GridFieldDataColumns(),
                    new GridFieldEditButton(),
                    new GridFieldDeleteAction(),
                    new GridFieldDetailForm()
                ))
            );
        }
        return $fields;
    }
    
    public function onBeforeWrite() {
        parent::onBeforeWrite();
		if (empty($this->Approved)) {
			if ($this->SignOff1ID > 0 && $this->SignOff2ID > 0) {
				if (!empty($this->SignedOff1) && !empty($this->SignedOff2)) {
					$this->Approved = date('Y-m-d H:i:s');
				}
			}
			elseif ($this->SignOff1ID > 0) {
				if (!empty($this->SignedOff1)) {
					$this->Approved = date('Y-m-d H:i:s');
				}
			}
		}
        if ($this->SignOff1ID < 1) {
            $this->Status = 'Submitted';
        }
        else {
            if (empty($this->Approved)) {
                $this->Status = 'Awaiting Sign Off';
            }
            else {
				$this->Status = $this->Archived ? 'Archived' : 'Signed Off';
            }
        }
    }
    
    public function canCreate($member = null) {
        return false;
    }
    
    public static function sign_off_fields() {
        $fields = singleton(get_called_class())->stat('summary_fields');
        unset($fields['SignOff1Summary']);
		unset($fields['SignOff2Summary']);
        return $fields;
    }
	
	public static function single_sign_off_fields() {
        $fields = singleton(get_called_class())->stat('summary_fields');
		$fields['SignOff1Summary'] = 'Sign Off';
		unset($fields['SignOff2Summary']);
        return $fields;
    }
	
	public static function signed_off_fields() {
        $fields = singleton(get_called_class())->stat('summary_fields');
		$fields['Approved.Nice'] = 'Approved';
        return $fields;
    }
	
    private $_days;
    
    public function Days() {
        if (is_null($this->_days)) {
            $this->_days = ArrayList::create();
            foreach (self::line_days($this->Type) as $day => $seconds) {
				$this->_days->push(DBField::create_field('Date', date('Y-m-d', $seconds)));
			}
        }
        return $this->_days;
    }
    
    private $_total_hours;
    
    public function getTotalHours() {
        if (is_null($this->_total_hours)) {
            try {
                $this->_total_hours = $this->Lines()->sum('HoursDays') + $this->Lines()->sum('HoursNights');
            }
            catch (LogicException $ex) {
                
            }
        }
        return $this->_total_hours;
    }
	
	private $_total_hours_days;
	
	public function getTotalHoursDays() {
		if (is_null($this->_total_hours_days)) {
            try {
                $this->_total_hours_days = $this->Lines()->sum('HoursDays');
            }
            catch (LogicException $ex) {
                
            }
        }
        return $this->_total_hours_days;
	}
	
	private $_total_hours_nights;
	
	public function getTotalHoursNights() {
		if (is_null($this->_total_hours_nights)) {
            try {
                $this->_total_hours_nights = $this->Lines()->sum('HoursNights');
            }
            catch (LogicException $ex) {
                
            }
        }
        return $this->_total_hours_nights;
	}
	
	private $_total_breaks;
	
	public function getTotalBreaks() {
		if (is_null($this->_total_breaks)) {
            try {
                $this->_total_breaks = $this->Lines()->sum('Breaks');
            }
            catch (LogicException $ex) {
                
            }
        }
        return $this->_total_breaks;
	}
	
	public function SignOff1Summary() {
		if ($this->SignOff1ID > 0) {
			$member = $this->SignOff1();
			if (empty($this->SignedOff1)) {
				return $member->getNameWithClient();
			}
			else {
				return DBField::create_field('HTMLText', sprintf(
					"%s<br />Signed Off: <strong>%s</strong>",
					$member->getNameWithClient(),
					date('d/m/Y H:i', strtotime($this->SignedOff1))
				));
			}
		}
		else {
			return ' ';
		}
	}
	
	public function SignOff2Summary() {
		if ($this->SignOff2ID > 0) {
			$member = $this->SignOff2();
			if (empty($this->SignedOff2)) {
				return $member->getNameWithClient();
			}
			else {
				return DBField::create_field('HTMLText', sprintf(
					"%s<br />Signed Off: <strong>%s</strong>",
					$member->getNameWithClient(),
					date('d/m/Y H:i', strtotime($this->SignedOff2))
				));
			}
		}
		else {
			return ' ';
		}
	}
	
	private $_output;
	
	public function output() {
		if (is_null($this->_output)) {
			$writer = PHPExcel_IOFactory::load(sprintf('%s/application/spreadsheets/timesheet.xlsx', $_SERVER['DOCUMENT_ROOT']));
			$sheet = $writer->setActiveSheetIndex(0);
			
			$sheet->setCellValue('C4', $this->CompanyName);
			$sheet->setCellValue('C5', $this->Location);
			$sheet->setCellValue('C6', date('d/m/Y', strtotime($this->EndDate)));
			$sheet->setCellValue('C7', $this->Name);
			if ($this->SignOff1ID > 0) {
				$sheet->setCellValue('C20', $this->SignOff1()->getNameWithClient());
				$sheet->setCellValue('G20', date('d/m/Y H:i', strtotime($this->SignedOff1)));
			}
			if ($this->SignOff2ID > 0) {
				$sheet->setCellValue('C21', $this->SignOff2()->getNameWithClient());
				$sheet->setCellValue('G21', date('d/m/Y H:i', strtotime($this->SignedOff2)));
			}
			
			$y = 12;
			for ($i = 6; $i >= 0; $i--) {
				$date = strtotime(sprintf('%s -%d days', $this->EndDate, $i));
				$sheet->setCellValue(sprintf('A%d', $y), date('l d/m/y', $date));
				foreach ($this->Lines() as $line) {
					if ($line->Date == date('Y-m-d', $date)) {
						$sheet->setCellValue(sprintf('B%d', $y), $line->JobTitle);
						$sheet->setCellValue(sprintf('D%d', $y), $line->JobNumber);
						$sheet->setCellValue(sprintf('E%d', $y), $line->Start);
						$sheet->setCellValue(sprintf('F%d', $y), $line->End);
						$sheet->setCellValue(sprintf('G%d', $y), $line->Breaks);
						$sheet->setCellValue(sprintf('H%d', $y), $line->HoursDays);
						$sheet->setCellValue(sprintf('I%d', $y), $line->HoursNights);
						break;
					}
				}
				$y++;
			}
			
			$sheet->setCellValue('C22', $this->CandidateNotes);
			
			$output = PHPExcel_IOFactory::createWriter($writer, 'Excel2007');
			
			ob_start();
			$output->save('php://output');
			$this->_output = ob_get_contents();
			ob_end_clean();
		}
		return $this->_output;
	}
}
