<?php


class HolidayRequest extends DataObject {
    
    private static $session_key = 'HolidayRequest';
    
    public static function retrieve() {
        return Compass_Registration::retrieve(get_called_class());
    }
    
    private static $db = array(
        'Section' => "Enum('Holiday Request,Complete')",
        'Type' => "Enum('Holiday Request')",
        
        /* Holiday Request */
        'Name' => 'Varchar(200)',
        'DOB' => 'Date',
        'JobTitle' => 'Varchar(200)',
        'Client' => 'Varchar(200)',
        'From' => 'Date',
        'To' => 'Date',
        'Days' => 'Varchar(200)',
        'ConfirmName' => 'Varchar(200)',
        'Email' => 'Varchar(254)',
        'ConfirmDOB' => 'Date',
        'Date' => 'Date'
    );
    
    private static $defaults = array(
        'Section' => 'Holiday Request'
    );
    private static $default_sort = 'Created DESC';
    
    private static $summary_fields = array(
        'Name' => 'Name',
        'Client' => 'Client',
        'Completed.Nice' => 'Date'
    );
    private static $searchable_fields = array(
        'Name', 'Client', 'Completed'
    );
    
    public function canCreate($member = null) {
		return false;
	}
    
    public function canEdit($member = null) {
		return false;
	}
    
    public function FullName() {
        return $this->Name;
    }
    
    public function OutputTitle() {
        return 'Holiday Request';
    }
}