<?php


class MedicalQuestionnaire extends DataObject {
    
    private static $session_key = 'MedicalQuestionnaire';
    
    public static function retrieve() {
        return Compass_Registration::retrieve(get_called_class());
    }
    
    private static $db = array(
        'Section' => "Enum('Medical History,Complete')",
        'Type' => "Enum('Medical Questionnaire')",
        
        /* Medical History */
        'Consciousness' => 'Boolean',
        'ConsciousnessDetails' => 'Text',
        'Bones' => 'Boolean',
        'BonesDetails' => 'Text',
        'Lungs' => 'Boolean',
        'LungsDetails' => 'Text',
        'Chest' => 'Boolean',
        'ChestDetails' => 'Text',
        'Vision' => 'Boolean',
        'VisionDetails' => 'Text',
        'Ears' => 'Boolean',
        'EarsDetails' => 'Text',
        'Metabolic' => 'Boolean',
        'MetabolicDetails' => 'Text',
        'Chronic' => 'Boolean',
        'ChronicDetails' => 'Text',
        'Treatment' => 'Boolean',
        'TreatmentDetails' => 'Text',
        'Disability' => 'Boolean',
        'DisabilityDetails' => 'Text',
        'Nights' => 'Boolean',
        'NightsDetails' => 'Text',
        'Medication' => 'Boolean',
        'MedicationDetails' => 'Text',
        'Convictions' => 'Boolean',
        'ConvictionsDetails' => 'Text',
        'DeclarationName' => 'Varchar(200)',
        'DeclarationDate' => 'Date',
        'EmergencyContactName' => 'Varchar(200)',
        'EmergencyContactPhone' => 'Varchar(20)',
        'EmergencyContactRelationship' => 'Varchar(200)'
    );
    
    private static $defaults = array(
        'Section' => 'Medical History'
    );
    private static $default_sort = 'Created DESC';
    
    private static $summary_fields = array(
        'DeclarationName' => 'Name',
        'Completed.Nice' => 'Date'
    );
    private static $searchable_fields = array(
        'DeclarationName', 'Completed'
    );
    
    public function canCreate($member = null) {
		return false;
	}
    
    public function canEdit($member = null) {
		return false;
	}
    
    public function FullName() {
        return $this->DeclarationName;
    }
    
    public function OutputTitle() {
        return 'Medical Questionnaire';
    }
}