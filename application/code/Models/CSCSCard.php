<?php


class CSCSCard extends DataObject {
    
    public static function form_map() {
        return CSCSCard::get()->map('ID', 'Name');
    }
    
    private static $db = array(
        'Name' => 'Varchar(200)'
    );
    private static $default_sort = 'Name ASC';
    private static $summary_fields = array(
        'Name' => 'Name'
    );
}