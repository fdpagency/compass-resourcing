<?php


class HomePageBlock extends DataObject {
    
    private static $db = array(
        'Title' => 'Varchar(200)',
		'CallToAction' => 'Varchar(200)',
        'LinkURL' => 'Varchar(200)',
        'Index' => 'Int'
    );
    private static $has_one = array(
        'Page' => 'HomePage',
        'Image' => 'Image',
        'LinkPage' => 'SiteTree',
        'LinkFile' => 'File'
    );
    private static $default_sort = 'Index ASC';
    public static $summary_fields = array( 
        'Title' => 'Title',
        'Image.CMSThumbnail' => 'Image',
        'Link' => 'Link'
    );
	
	public function Link($action = null) {
		if ($this->LinkPageID > 0) {
			return $this->LinkPage()->Link($action);
		}
		else if ($this->LinkFileID > 0) {
			return sprintf('%s%s', $this->LinkFile()->URL, $action);
		}
		else {
			return $this->LinkURL;
		}
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->removeByName('Index');
		$fields->removeByName('PageID');
		$fields->fieldByName('Root.Main.Image')->setFolderName('HomePage');
		$fields->addFieldToTab(
    		'Root.Main',
    		TreeDropdownField::create('LinkPageID', 'Link Page', 'SiteTree'),
    		'LinkFile'
		);
        return $fields;
	}
	
	protected function onBeforeWrite() {
        if (!$this->Index) {
            $this->Index = HomePageBlock::get()->filter('PageID', $this->PageID)->max('Index') + 1;
        }
        parent::onBeforeWrite();
    }
	
	public function canView($member = null) {
        return $this->Page()->canView($member);
    }
    
    public function canCreate($member = null) {
        return $this->Page()->canCreate($member);
    }
    
    public function canEdit($member = null) {
        return $this->Page()->canEdit($member);
    }
    
    public function canDelete($member = null) {
        return $this->Page()->canDelete($member);
    }
}