<?php

class Compass_DOMPDF extends DOMPDF {
    
    private $_data;
    
    public function __construct($data) {
        parent::__construct();
        $this->_data = $data;
    }
    
    public function __get($key) {
        switch ($key) {
            case 'Filename':
				switch ($this->_data->ClassName) {
					case 'RegistrationPack':
						return sprintf('%s-%s-%s.pdf', preg_replace('/([^A-Za-z0-9]*)/', '', $this->_data->FullName()), $this->_data->Type, date('dmY', strtotime($this->_data->Completed)));
						break;
					case 'Terms':
						return sprintf('CompassTerms-V%d-%s.pdf', $this->_data->Version, date('dmY', strtotime($this->_data->Created)));
						break;
					default:
						return sprintf('%s-%s-%s.pdf', $this->_data->ClassName, preg_replace('/([^A-Za-z0-9]*)/', '', $this->_data->Name), date('dmY', strtotime($this->_data->Created)));
						break;
				}
                break;
            default:
                return parent::__get($key);
                break;
        }
    }
    
    public function stream($filename = null, $options = null) {
        return parent::stream(is_null($filename) ? $this->Filename : $filename);
    }
}