<?php

global $project;
$project = 'application';

require_once('conf/ConfigureFromEnv.php');

i18n::set_locale('en_GB');

Requirements::set_force_js_to_bottom(true);

GD::set_default_quality(90);

Email::set_mailer(new SmtpMailer());

Object::useCustomClass('MemberLoginForm', 'Compass_MemberLoginForm');

require(sprintf('%s/vendor/autoload.php', $_SERVER['DOCUMENT_ROOT']));
define('DOMPDF_ENABLE_AUTOLOAD', false);
define('DOMPDF_ENABLE_REMOTE', true);
require_once(sprintf('%s/vendor/dompdf/dompdf/dompdf_config.inc.php', $_SERVER['DOCUMENT_ROOT']));
