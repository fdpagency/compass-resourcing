<table cellpadding="0" cellspacing="0">
    <tr>
        <td><h2>A quick labour order has been received from $Name</h2></td>
    </tr>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" class="summary">
                <tr>
                    <th>Name</th>
                    <td>$Name</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td>$Email</td>
                </tr>
                <% if $Phone %>
                    <tr>
                        <th>Phone</th>
                        <td>$Phone</td>
                    </tr>
                <% end_if %>
				<tr>
					<th>Comments</th>
					<td>$Comments</td>
				</tr>
                <tr>
                    <th>Date / Time</th>
                    <td>$Now.Format('d/m/Y H:i')</td>
                </tr>
            </table>
        </td>
    </tr>
</table>