<table cellpadding="0" cellspacing="0">
    <tr>
        <td><h2>$FullName has submitted a medical questionnaire</h2></td>
    </tr>
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" class="summary">
                <tr>
                    <th>Name</th>
                    <td>$FullName</td>
                </tr>
                <tr>
                    <th>Date / Time</th>
                    <td>$Completed.Format('d/m/Y H:i')</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="padded"><p>All submitted registration information is attached to this email. You can also download a medical questionnaire PDF from the Compass website administration area.</p></td>
    </tr>
</table>