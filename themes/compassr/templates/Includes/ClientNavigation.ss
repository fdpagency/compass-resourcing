<ul id="subnav">
	<li>Client Menu:</li>
	<% with $ClientDashboardPage %>
		<li<% if $Top.ClassName == 'ClientDashboardPage' %> class="current"<% end_if %>><a href="$Link">Dashboard</a></li>
		<% loop $Children %>
			<li<% if $Current %> class="current"<% end_if %>><a href="$Link">$MenuTitle</a></li>
		<% end_loop %>
	<% end_with %>
	<li class="logout"><a href="$Link('logout')">Logout</a></li>
</ul>