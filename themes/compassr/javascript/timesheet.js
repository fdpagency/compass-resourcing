$(document).ready(function(){
    $('table tbody tr').each(function() {
        day = $('td.date div.title', this).text();
        dropdown = $('<select id="copy_to_' + day.toLowerCase() + '"><option value="">Copy from</option></select>');
        $('table tbody tr:not(.footer)').each(function() {
            d = $('td.date div.title', this).text();
            if (d != day) {
                dropdown.append('<option value="' + d.toLowerCase() + '">' + d + '</option>')
            }
        });
        dropdown.change(function() {
            if ($(this).val() != '') {
                row = $(this).parents('tr');
                current = $('td.date div.title', row).text().toLowerCase();
                source = $(this).val();
                $('input, select', row).each(function() {
                    if ($(this).attr('name') !== undefined) {
                        if ($(this).attr('type') == 'checkbox') {
                            $(this).prop('checked', $('*[name="' + $(this).attr('name').replace(current, source) + '"]').prop('checked'));
                        }
                        else {
                            $(this).val($('*[name="' + $(this).attr('name').replace(current, source) + '"]').val());    
                        }
                        $(this).change();
                    }
                });
                $(this).val('');
            }
        });
        $('div.copy', this).append(dropdown);
    });
    
    fields = ['day_hours', 'night_hours', 'breaks'];
    for (i=0; i<fields.length; i++) {
        field = fields[i];
        $('table tr > td input[name$="' + field + '"]').attr('class', field).change(function(){
            total = 0;
            $('table tr:not(.footer) > td input[name$="' + $(this).attr('class') + '"]').each(function() {
                value = parseFloat($(this).val());
                if (!isNaN(value)) total += value;
                else value = '';
                $(this).val(value);
            });
            $('tr.footer input[name$="' + $(this).attr('class') + '"]').val(total)
        }).change();
    }
});