String.prototype.startsWith = function(prefix) {
    return this.indexOf(prefix) === 0;
}

String.prototype.endsWith = function(suffix) {
    return this.match(suffix+"$") == suffix;
};

$(document).ready(function() {
    $('input[name*=cis__role]').change(function() {
        suffix = '_b';
        if (!$(this).attr('name').endsWith('_a')) {
            suffix = '_a';
        }
        if ($(this).val() != '') {
            $('input[name$=cis__role' + suffix + '][value=""]').prop('checked', true);
        }
        update_responsibilities();
    });
    update_responsibilities();
    
    $('input[name*=cis__role]').parents('form').submit(function() {
        $('div.responsibilitiesfield ul li:not(.active) input:checked').prop('checked', false);
    });
    
    $('input[name$=has_utr_number]').change(function() {
        $('.notice', $(this).parents('div.form_row')).remove();
        $('.form_row.textfield.hidden, input.hidden').show().removeClass('hidden');
        if ($('input[name="' + $(this).attr('name') + '"]:checked').val() == '0') {
            $(this).parents('div.form_row').append('<p class="notice">In order to complete this form you must have a UTR number.</p>');
            $('.form_row.textfield, input.next').hide().addClass('hidden');
        }
    }).change();
    
});

function update_responsibilities() {
    if ($('input[name*="cis__role_b"]:checked').length > 0) {
        role = $('input[name*="cis__role_b"]:checked').val();
        $('div.responsibilitiesfield ul li').hide().removeClass('active');
        if (role.length > 0) {
            $('div.responsibilitiesfield ul li').each(function() {
                roles = $(this).attr('data-roles').split(',');
    			if (jQuery.inArray(role, roles) > -1) {
        			$(this).show().addClass('active');
    			}
            });
        }
        $('div.responsibilitiesfield').toggle($('div.responsibilitiesfield ul li.active').length > 0);
    }
}