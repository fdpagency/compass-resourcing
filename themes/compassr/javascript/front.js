String.prototype.startsWith = function(prefix) {
    return this.indexOf(prefix) === 0;
}

String.prototype.endsWith = function(suffix) {
    return this.match(suffix+"$") == suffix;
};

$(document).ready(function() {

    var menu = $('nav');
    var breakPoint = 700;

    $('#menuToggle').click(function(e){
        e.preventDefault();
        menu.slideToggle();
    });

    $('.sidebarItem > h3').click(function(e){
        e.preventDefault();
        if($(window).width() < breakPoint) {
            $(this).toggleClass('active');
            $(this).parent().children('.formContainer').slideToggle();
        }
    });
    
    jQuery.support.placeholder = (function(){
	    var i = document.createElement('input');
		return 'placeholder' in i;
	})();
	
	$(document).ready(function(){
		$('#sidebarForm input[type=text]').each(function(){
			if ($.support.placeholder) {
				$(this).attr('placeholder', $(this).parent().children('label').text());
			}
			else {
				$(this).focus(function(){
					if ($(this).val() == $(this).parent().children('label').text()) $(this).val('');
				}).blur(function(){
					if ($(this).val().length < 1) $(this).val($(this).parent().children('label').text());
				}).trigger('blur');
			}
		});
	});
	
	$('#quickEnquiryButton').click(function(){
		$('#quickEnquiryContainer').toggleClass('active');  
	});
	
	$('#quickEnquiryClose').click(function(){
		$('#quickEnquiryContainer').removeClass('active');  
	});


    $(window).resize(function(){
        var w = $(window).width();
        if($(window).width() > breakPoint && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
        if($(window).width() > breakPoint && $('.formContainer').is(':hidden')) {
            $('.formContainer').removeAttr('style');
        }	
        if($(window).width() > breakPoint) {
            $('.sidebarItem > h3').removeClass('active');
        }
    });
    
    $('form input.date').datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: 'c-80:c+10'
    });
    
    $('form div.construction').toggle($('form input[name="WorkInConstruction"][value="1"]').prop('checked'));
    
    $('form input[name="WorkInConstruction"]').change(function() {
        $('form div.construction').slideToggle($('input[value="1"]', $(this).parent()).prop('checked'));
    });
    
    $('.payment_method input').change(function() {
        $('.payment_method input').each(function() {
            if ($(this).prop('checked')) {
                $(this).siblings('label').animate({'opacity' : 1}, 200, function() {
                    $(this).removeClass('faded');
                });
            }
            else {
                if ($('.payment_method input:checked').length > 0) {
                    $(this).siblings('label').animate({'opacity' : 0.5}, 200, function() {
                        $(this).addClass('faded');
                    });
                }
            }
        });
    }).change();
    
    $('input[name=WorkPermit]').change(function() {
        $('.notice', $(this).parents('div.form_row')).remove();
        $('.form_row.textfield.hidden, input.hidden').show().removeClass('hidden');
        if ($('input[name="' + $(this).attr('name') + '"]:checked').val() == '1') {
            $(this).parents('div.form_row').append('<p class="notice">We require a legible copy of the front and back of your permit / visa before sending you to work</p>');
        }
    }).change();
    
    $('form select[name$=__self_employment_type_id]').change(function() {
        labels = $('label:contains("Payment System")');
        selected = $('option', this).index($('option:selected', this)) - 1;
        labels.each(function() {
            $(this).parents('div.form_row').toggle(labels.index($(this)) == selected);
        });
    }).change();
    
    $('a.popup').magnificPopup({
        type: 'inline',
        midClick: true
    }).click(function() {
        $('div.button_row input.next').prop('disabled', false).removeClass('disabled');
    });
    
    if ($('div.eligibility a').length > 0) {
        $('div.button_row input.next').prop('disabled', true).addClass('disabled');
    }
    
    $('input[name*=CISRole]').change(function() {
        suffix = 'B';
        if (!$(this).attr('name').endsWith('A')) {
            suffix = 'A';
        }
        if ($(this).val() != '') {
            $('input[name$=CISRole' + suffix + '][value=""]').prop('checked', true);
        }
        update_responsibilities();
    });
    update_responsibilities();
    
    $('input[name*=CISRole]').parents('form').submit(function() {
        $('div.checkboxset ul li:not(.active) input:checked').prop('checked', false);
    });
    
    $('input[name$=HaveUTRNumber]').change(function() {
        $('.notice', $(this).parents('div.form_row')).remove();
        $('div.registrationForm .form_row.text.hidden, input.hidden').show().removeClass('hidden');
        if ($('div.registrationForm input[name="' + $(this).attr('name') + '"]:checked').val() == '0') {
            $(this).parents('div.form_row').append('<p class="notice">In order to complete this form you must have a UTR number.</p>');
            $('div.registrationForm .form_row.text, form.registration input.next').hide().addClass('hidden');
        }
    }).change();
});

function update_responsibilities() {
    if ($('input[name*="CISRoleB"]:checked').length > 0) {
        role = $('input[name*="CISRoleB"]:checked').val();
        $('div.checkboxset ul li').hide().removeClass('active');
        if (role.length > 0) {
            $('div.checkboxset ul li').each(function() {
                roles = $(this).attr('data-roles').split(',');
    			if (jQuery.inArray(role, roles) > -1) {
        			$(this).show().addClass('active');
    			}
            });
        }
        $('div.checkboxset').toggle($('div.checkboxset ul li.active').length > 0);
    }
}