$(document).ready(function() {
    $('input[type=file]').change(function() {
        upload_set = false;
        $('input[type=file]').each(function() {
            if ($(this).val() != '') {
                upload_set = true;
            }
        });
        $('input[name=next]').toggle(!upload_set);
    });
});