var fields = ['HoursDays', 'HoursNights'];

$(document).ready(function(){
    for (i=0; i<fields.length; i++) {
        field = fields[i];
        $('#timesheetContainer div.days input[name$="' + field + '"]').attr('class', field).change(function(){
            update_counts($(this).attr('class'));
        }).change();
    }
    
    $('div.day:not(.example) input[name$=Day][type=checkbox]').change(function() {
        if (!$(this).parents('form').hasClass('readonly')) {
			disabled = !$(this).prop('checked');
			$(this).parents('div.day').toggleClass('disabled', disabled);
			$('input[type=text], select', $(this).parents('div.day')).prop('disabled', disabled).toggleClass('disabled', disabled);
		}
        for (i=0; i<fields.length; i++) {
            update_counts(fields[i]);
        }
    }).change();
    
});

function update_counts(field) {
    total = 0;
    $('#timesheetContainer div.days div.day:not(.example) input[name$="' + field + '"]').each(function() {
		if ($('input[type=checkbox]', $(this).parents('div.day').children('div.date')).prop('checked')) {
			value = parseFloat($(this).val());
			if (!isNaN(value)) total += value;
			else value = '';
			$(this).val(value);
		}
    });
    $('#timesheetContainer div.footer input[name$="' + field + '"]').val(total)
}
