    jQuery.support.placeholder = (function(){
        var i = document.createElement('input');
        return 'placeholder' in i;
    })();

    $(document).ready(function(){
    	$('#homepageForm input[type=text]').each(function(){
    		if ($.support.placeholder) {
    			$(this).attr('placeholder', $(this).parent().children('label').text());
    		}
    		else {
    			$(this).focus(function(){
    				if ($(this).val() == $(this).parent().children('label').text()) $(this).val('');
    			}).blur(function(){
    				if ($(this).val().length < 1) $(this).val($(this).parent().children('label').text());
    			}).trigger('blur');
    		}
    	});

    	var classArray = ['one', 'two', 'three', 'four', 'five'];
    	var randomImage = classArray[Math.floor(Math.random() * classArray.length)];
    	$('#backgroundImage').addClass(randomImage);
    });

