$(document).ready(function() {
    $('#uk_locations').each(function() {
        map = $('<div id="uk_locations_map"></div>');
        $(' > li', this).each(function() {
            map.append('<div id="' + $(this).attr('class') + '">' + $(this).attr('class') + '</div>');
            $(this).hover(function() {
                $('#' + $(this).attr('class')).addClass('on');
            }, function() {
                $('#' + $(this).attr('class')).removeClass('on');
            });
        });
        $(this).after(map);
    });
});